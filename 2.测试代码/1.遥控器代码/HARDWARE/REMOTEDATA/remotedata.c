#include "sys.h"



int16_t THROTTLE;//油门反馈参数
FLOAT_ANGLE Angle;//姿态参数



void Recdata(void)
{
			NRF24L01_RX_Mode();	//切换到接收模式
			NRF24L01_RxPacket(NRF24L01_RX_DATA);	
}

void Remote_Data_ReceiveAnalysis(void)//数据解析
{
			if(NRF24L01_RX_DATA[0]==0xFF)//帧头
			{
					if(NRF24L01_RX_DATA[1]==0x00)//校验位
					{
							
							THROTTLE      = NRF24L01_RX_DATA[2]<<8|NRF24L01_RX_DATA[3];		//油门
							Angle.yaw = (NRF24L01_RX_DATA[4]<<8|NRF24L01_RX_DATA[5])/100; 		//偏航
							Angle.pit     = (NRF24L01_RX_DATA[6]<<8|NRF24L01_RX_DATA[7])/100;	  	//俯仰
							Angle.rol    = (NRF24L01_RX_DATA[8]<<8|NRF24L01_RX_DATA[9])/100;		//翻滚

					}
			}
}
/**************************************************************************************************************
* 函  数：void SendToRemote(void)
* 功  能：飞机状态数据发送给遥控器
* 参  数：无
* 返回值：无
* 备  注：注意：SI24R1单次发送最大32个字节，请勿越界
***************************************************************************************************************/
void SendToRemote(void)
{
	int16_t temp; 	
	static uint8_t ID=0;
	ID++;
	NRF24L01_TX_DATA[0] = 0xFF;//帧头
	
	NRF24L01_TX_DATA[1] = 0x00; //标志位组
	
	temp = (u16)Tele_x0; //油门
	NRF24L01_TX_DATA[2] = BYTE1(temp);
	NRF24L01_TX_DATA[3] = BYTE0(temp);
	temp = (int)Tele_y0; //航向
	NRF24L01_TX_DATA[4] = BYTE1(temp);
	NRF24L01_TX_DATA[5] = BYTE0(temp);
	temp = (int)Tele_x1; //俯仰
	NRF24L01_TX_DATA[6] = BYTE1(temp);
	NRF24L01_TX_DATA[7] = BYTE0(temp);
	temp = (int)Tele_y1; //横滚
	NRF24L01_TX_DATA[8] = BYTE1(temp);
	NRF24L01_TX_DATA[9] = BYTE0(temp);
	NRF24L01_TX_DATA[10] = ID;
	
	NRF24L01_TxPackets(NRF24L01_TX_DATA); //发送函数
}

