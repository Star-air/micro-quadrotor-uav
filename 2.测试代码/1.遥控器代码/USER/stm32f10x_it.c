/**
  ******************************************************************************
  * @file    GPIO/IOToggle/stm32f10x_it.c 
  * @author  MCD Application Team
  * @version V3.5.0
  * @date    08-April-2011
  * @brief   Main Interrupt Service Routines.
  *          This file provides template for all exceptions handler and peripherals
  *          interrupt service routine.
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2011 STMicroelectronics</center></h2>
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stm32f10x_it.h" 
#include "sys.h"

uint8_t LED_Scan = 0;
uint8_t IMU_Scan = 0;
uint8_t IRQ_Scan = 0;
uint8_t Batt_Scan = 0;


 
 
 
/****************************************************************************************************
* 函  数: void TIM4_IRQHandler(void) 
* 功  能: TIM4定时器中断，1ms进一次中断也就是1000Hz
* 参  数: 无
* 返回值: 无
* 备  注: 此函数是整个程序的运行时基，不同的中断时间对应不同频率；
*         对于一些计算对调用时间要求比较严格时可用此方法；
*         扫描频率 = 1000Hz/分频系数；
****************************************************************************************************/
void TIM1_UP_IRQHandler(void)   //TIM4中断服务函数
{
	static uint16_t ms10 = 0,ms100 = 0,ms200 = 0,ms400 = 0; //分频系数
	if(TIM_GetITStatus(TIM1,TIM_IT_Update) != RESET)	//判断是否进入TIM更新中断
	{

		
		ms10++;		
		ms100++;
		ms200++;
		ms400++;


		if(ms10 >= 10)//100Hz
		{
			ms10 = 0;
			IMU_Scan = 1;
		}
		if(ms100 >= 100)//10Hz
		{
			ms100 = 0;
			LED_Scan = 1;
		}
		if(ms200 >= 200)//5Hz
		{
			ms200 = 0;
			IRQ_Scan = 1;
		}
		if(ms400 >= 400)//2.5Hz
		{
			ms400 = 0;
			Batt_Scan = 1;
		}
	}
	TIM_ClearITPendingBit(TIM1,TIM_IT_Update);	//清除TIM4更新中断
} 
 
 
 
 
 
  void EXTI1_IRQHandler()
{
			
				uint8_t sta;
				if(EXTI_GetITStatus(EXTI_Line1) != RESET )
				{
					NRF24L01_CE=0;//拉低CE，以便读取NRF中STATUS中的数据
					sta=NRF24L01_Read_Reg(STATUS); //读取STATUS中的数据，以便判断是由什么中断源触发的IRQ中断
					//OLED_ShowString(20,5,"1",16);
					/* 发送完成中断 TX_OK */
					if(sta & TX_OK)                                   
					{										
						NRF24L01_RX_Mode();//切换到接收模式
						NRF24L01_Write_Reg(NRF_WRITE_REG+STATUS,TX_OK); //清除TX_DS或MAX_RT中断标志
						NRF24L01_Write_Reg(FLUSH_TX,0xff);//清除TX FIFO寄存器 
						//OLED_ShowString(0,0,"Sent OK",6);//printf("Sent OK!!!!\r\n");
					}
					/* 接收完成中断 RX_OK */
					if(sta & RX_OK) 
					{	
						
						NRF24L01_RxPackets(NRF24L01_RX_DATA);	
						Remote_Data_ReceiveAnalysis();
						NRF24L01_Write_Reg(NRF_WRITE_REG+STATUS,RX_OK); //清除发送完成标志
						//OLED_ShowString(20,5,"Receive OK",16);//printf("Receive OK!!!!\r\n");	
					}
					/* 达到最大重发次数中断  MAX_TX */
					if(sta & MAX_TX)                                  
					{											
						NRF24L01_TX_Mode();	//切换到发送模式
						NRF24L01_Write_Reg(NRF_WRITE_REG+STATUS,MAX_TX);//清除接达到最大重发标志
						NRF24L01_Write_Reg(FLUSH_TX,0xff);//清除TX_FIFO
						//printf("Sent Max Data!!!\r\n"); 
					}
					EXTI_ClearITPendingBit(EXTI_Line1);
				}				

		
	

}
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
void NMI_Handler(void)
{
}
 
void HardFault_Handler(void)
{
  /* Go to infinite loop when Hard Fault exception occurs */
  while (1)
  {
  }
}
 
void MemManage_Handler(void)
{
  /* Go to infinite loop when Memory Manage exception occurs */
  while (1)
  {
  }
}

 
void BusFault_Handler(void)
{
  /* Go to infinite loop when Bus Fault exception occurs */
  while (1)
  {
  }
}
 
void UsageFault_Handler(void)
{
  /* Go to infinite loop when Usage Fault exception occurs */
  while (1)
  {
  }
}
 
void SVC_Handler(void)
{
}
 
void DebugMon_Handler(void)
{
}
 
void PendSV_Handler(void)
{
}
 
void SysTick_Handler(void)
{
}

/******************************************************************************/
/*                 STM32F10x Peripherals Interrupt Handlers                   */
/*  Add here the Interrupt Handler for the used peripheral(s) (PPP), for the  */
/*  available peripheral interrupt handler's name please refer to the startup */
/*  file (startup_stm32f10x_xx.s).                                            */
/******************************************************************************/
