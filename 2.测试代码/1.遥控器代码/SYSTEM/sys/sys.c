#include "sys.h"
int16_t Tele_x0,Tele_y0,Tele_x1,Tele_y1;
int16_t ADC_x0,ADC_y0,ADC_x1,ADC_y1;
#define pitch_hold_value 2007
#define roll_hold_value 2024
#define yaw_hold_value 2073
  /*****************************************************************************
* 函  数：void NvicConfig(void)
* 功  能：配置工程中所有中断的优先级
* 参  数：无
* 返回值：无
* 备  注：此优先级中断不要随便更改哦
*****************************************************************************/
void NVIC_Config(void)
{
	NVIC_InitTypeDef NVIC_InitStruct;
	
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	
	NVIC_InitStruct.NVIC_IRQChannel=EXTI1_IRQn;   //配置外部中断通道
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority=2;   //设置抢占优先级为0
	NVIC_InitStruct.NVIC_IRQChannelSubPriority=2;   //设置子优先级为1
	NVIC_InitStruct.NVIC_IRQChannelCmd=ENABLE;   //使能外部中断通道
	NVIC_Init(&NVIC_InitStruct);   //中断优先级初始化函数
}


/*****************************************************************************
* 函  数：void TIM_Init(void)
* 功  能：TIM4初始化为1ms计数一次
* 参  数：无
* 返回值：无
* 备  注：更新中断时间 Tout = (ARR-1)*(PSC-1)/CK_INT
*****************************************************************************/
void TIM_Init(void)
{
	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;   //定义定时器结构体变量
	NVIC_InitTypeDef NVIC_InitStructure;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1,ENABLE);
	TIM_DeInit(TIM1); 
	
	
	TIM_TimeBaseInitStruct.TIM_Period = 720-1;
	TIM_TimeBaseInitStruct.TIM_Prescaler = 100-1;
	TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInitStruct.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseInitStruct.TIM_RepetitionCounter = 0;
	TIM_TimeBaseInit(TIM1,&TIM_TimeBaseInitStruct);

	NVIC_InitStructure.NVIC_IRQChannel = TIM1_UP_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	TIM_ClearFlag(TIM1, TIM_FLAG_Update); 
	TIM_ITConfig(TIM1,TIM_IT_Update,ENABLE);

	TIM_Cmd(TIM1,ENABLE);
//	TIM_ITConfig(TIM4,TIM_IT_Update,DISABLE);   //TIM4中断使能
//	
//	TIM_Cmd(TIM4,DISABLE);   //TIM4使能
}


 
/* 
	系统初始化函数
 */
void system_Init(void)
{	
		//uint8 res;
/*************延时函数初始化*********************/	
		delay_init();//延时函数初始化
		NVIC_Config();//优先级设置
		TIM_Init();//程序时基
/*************OLED初始化*********************/	
		OLED_Init();//OLED初始化
		OLED_Clear();
		OLED_ShowString(20,3,"WAIT...",16);
		Adc_Init();
		Exit_Init();
		
		NRF24L01_Init();
		while(NRF24L01_Check())//检测NRF24L01是否存在
		{
				OLED_ShowString(20,3,"NRF_ERROR",16);
		}
		OLED_ShowString(20,5,"NRF_ok   ",16);
		NRF24L01_RX_Mode();//接收模式
		Tele_x0=Get_Adc_Average(0,5)*255/4096;
		while(!(Tele_x0<=10))
		{
				Tele_x0=Get_Adc_Average(0,5)*255/4096;
		}
		__enable_irq();//开总中断
		OLED_Clear();
}


void xianfu(int16_t *temp,int16_t ADC,int16_t zhongzhi,int16_t yingshe)
{
		if(*temp>zhongzhi)
		{
				*temp=yingshe+(ADC-zhongzhi)*yingshe/(4200-zhongzhi);

		}
		else if(*temp<zhongzhi)
		{
				*temp=yingshe-(zhongzhi-ADC)*yingshe/(zhongzhi-0);
		}
		else{

		}
		if(*temp>2*yingshe)
		{
				*temp=2*yingshe;
		}	
}


	/* 
	任务调度函数
 */
void Task_Schedule(void)
{
			float pit,rol,yaw;
//				int16_t temp; 	
//				NRF24L01_Test();
			//Recdata();//接收数据
			OLED_ShowNum(0,0,THROTTLE,4,16);	
			OLED_ShowNum(0,2,Angle.pit,4,16);	
			OLED_ShowNum(0,4,Angle.rol,4,16);	
			OLED_ShowNum(0,6,Angle.yaw,4,16);	
//			OLED_ShowNum(0,2,(Tele_x1)/10,4,16);	
//			OLED_ShowNum(0,4,(Tele_y1)/10,4,16);	
//			OLED_ShowNum(0,6,(Tele_y0)/10,4,16);	
//			
//			pit=(u8)(-(Tele_x1-250)/10.0f);
//			rol=(u8)(-(Tele_y1-250)/10.0f);
//			yaw=(u8)((Tele_y0-1800)/10.0f);
			ADC_x0=Get_Adc_Average(0,5);//油门
			ADC_y0=Get_Adc_Average(1,5);//偏航
			ADC_x1=Get_Adc_Average(2,5);//俯仰
			ADC_y1=Get_Adc_Average(3,5);//翻滚*255/4096
			Tele_x0=ADC_x0;
			xianfu(&Tele_y0,ADC_y0,2073,1800);
			xianfu(&Tele_x1,ADC_x1,2007,250);
			xianfu(&Tele_y1,ADC_y1,2024,250);

			if(IRQ_Scan)
			{
					IRQ_Scan=0;
					//获取ADC参数

					SendToRemote();//发送数据
			}
			
		
}


