#include "sys.h"
uint8_t key=0;
uint8_t UI_flag = 1;//UI显示标志位 1打开，0关闭
uint8_t Fly_flag = 0;//起飞模式标志位，1打开，0关闭
uint8_t UI_Refresh = 0;//一些页面需要不断执行
uint8_t suo = 1;//飞机锁
//static u8 flag=1;
int Setpwm=0;
u8 tmp_buf[33]; 
/* 
	系统初始化函数
 */
void system_Init(void)
{	
		//u8 res;

/*************中断优先级设置*********************/	
		NVIC_Config(); 
/*************延时函数初始化*********************/	
		delay_init();//延时函数初始化
/*************定时器初始化*********************/			
		TIM_Init();//程序时基
/*************外部中断初始化*********************/		

		//Exit_Init();//IRQ触发中断
/*************OLED初始化*********************/	
		OLED_Init();//OLED初始化
		OLED_Clear();//清屏
/*************LED初始化*********************/	
		LED_Init();
/*************uart初始化*********************/	
		uart1_init(9600);	 //串口初始化为9600
/*************电机初始化*********************/		
		MOTOR_Init();
		Moto_Pwm(0,0,0,0);//关掉四个电机
/*************MPU初始化*********************/	
		MPU_Init();//MPU初始化
		__disable_irq();//关总中断	
		while(mpu_dmp_init())//DMP初始化
		{
			//res = mpu_dmp_init();
			OLED_ShowString(0,0,"MPU6050 ERROR",6);
			delay_ms(200);
		} 
		OLED_ShowString(0,0,"MPU6050 OK!",6);
		__enable_irq();//开总中断
/*************NRF初始化*********************/	
		NRF24L01_Init();//NRF初始化
		while(NRF24L01_Check())//检测NRF24L01是否存在
		{
				OLED_ShowString(0,0,"NRF ERROR",6);
		}
		OLED_ShowString(0,0,"NRF OK",6);
		NRF24L01_RX_Mode();//接收模式
/*************电压测量初始化*********************/	
		BATT_Init();

/*************PID参数初始化*********************/			
		PID_controllerInit();
		
		
		if(UI_flag)//如果先执行UI
		{
				Key_Init();//按键初始化
				OLED_Clear();//清屏
				showUI(0);//显示初始界面
				LED=1;//点亮灯
		}

		
		
		
}


/* 
	任务调度函数
 */
void Task_Schedule(void)
{
		if(UI_flag)//UI显示 刷按键
		{
				myKey_GetKeyValue();
				if(key_Value.longPressed==1||key_Value.shortPressed==1||key_Value.doublePressed==1)//按键更新
				{
						if(key_Value.shortPressed==1)
						{
								key = 1;
								key_Value.shortPressed=0;
								showUI(key);
						}
						else if(key_Value.doublePressed==1)
						{
								key = 2;
								key_Value.doublePressed=0;
								showUI(key);
						}
						else if(key_Value.longPressed==1)
						{
								key = 3;
								key_Value.longPressed=0;
								showUI(key);
						}
						else
						{}
				}
				if(UI_Refresh)
				{
					showUI(0);//不断刷当前页面
				}
				
		}
		
		if(Fly_flag)//飞行模式
		{
//					if(flag==1)
//					{
//							flag=0;
//							OLED_Clear();//清屏
//							OLED_ShowString(0,0,"pi_kp:",6);
//							OLED_ShowString(0,1,"pi_ki:",6);
//							OLED_ShowString(0,2,"pi_kd:",6);
//							OLED_ShowString(0,3,"po_kp:",6);
//					}
////					

						//Recdata();//接收数据
//						OLED_ShowNum(0,0,RC_Control.THROTTLE,4,16);	
//						OLED_ShowNum(0,2,RC_Control.PITCH,4,16);	
//						OLED_ShowNum(0,4,RC_Control.ROLL,4,16);	
//						OLED_ShowNum(0,6,RC_Control.YAW,4,16);		

//					OLED_ShowNum(0,0,RC_Control.THROTTLE,4,16);	
//					OLED_ShowNum(0,2,Att_Angle.yaw*100,4,16);	
//					OLED_ShowNum(0,4,Att_Angle.pit*100,4,16);	
//					OLED_ShowNum(0,6,Att_Angle.rol*100,4,16);		
					
			
					if(IMU_Scan) //100Hz 控制
					{ 
						IMU_Scan=0;
						Prepare_Data();//读取DMP数据和原始数据
						IMUupdate(&Gyr_rad,&Acc_filt,&Att_Angle);
						Control(suo);					
					}
					
					if(LED_Scan) //10Hz 工作闪灯
					{
						LED_Scan=0;
						LED=~LED;

					}
					if(IRQ_Scan) //5Hz
					{
						IRQ_Scan = 0;
						SingalCheck();//断连检测
						SendToRemote();//发送数据
						if(connected_flag)
						{
								OLED_ShowString(0,0,"CON SUCCESS ",16);
						}
						else
						{
								OLED_ShowString(0,0,"CON FALSE   ",16);
						}

					}
					if(Batt_Scan) //2.5Hz
					{
						Batt_Scan = 0;
//						printf("vx=%0.2f vy=%0.2f vz=%0.2f\r\n",Att_Angle.pit,Att_Angle.rol,Att_Angle.yaw); 
//						ANO_DT_Send_Status(Att_Angle.rol,Att_Angle.pit,Att_Angle.yaw,0,0,0);
//						ANO_DT_Send_Senser(Acc_filt.X,Acc_filt.Y,Acc_filt.Z,Gyr_rad.X,Gyr_rad.Y,Gyr_rad.Z,0,0,0,0);
//						OLED_ShowNum(52,0,(u8)(pid_out_Pitch.kp),1,6);
//						OLED_ShowNum(52,1,(u8)(pid_out_Pitch.ki),1,6);
//						OLED_ShowNum(52,2,(u8)(pid_out_Pitch.kd),1,6);
//						OLED_ShowString(60,0,".",6);
//						OLED_ShowString(60,1,".",6);
//						OLED_ShowString(60,2,".",6);
//						OLED_ShowNum(68,0,(u8)((int)(pid_out_Pitch.kp * (float)10)%(int)10),1,6);
//						OLED_ShowNum(68,1,(u8)((int)(pid_out_Pitch.ki * (float)10)%(int)10),1,6);
//						OLED_ShowNum(68,2,(u8)((int)(pid_out_Pitch.kd * (float)10)%(int)10),1,6);
						
//						OLED_ShowNum(52,0,(u8)(pid_in_Roll.kp),1,6);
//						OLED_ShowNum(52,1,(u8)(pid_in_Roll.ki),1,6);
//						OLED_ShowNum(52,2,(u8)(pid_in_Roll.kd),1,6);
//						OLED_ShowNum(52,3,(u8)(pid_out_Roll.kp),1,6);
//						OLED_ShowString(60,0,".",6);
//						OLED_ShowString(60,1,".",6);
//						OLED_ShowString(60,2,".",6);
//						OLED_ShowString(60,3,".",6);
//						OLED_ShowNum(68,0,(u8)((int)(pid_in_Roll.kp * (float)10)%(int)10),1,6);
//						OLED_ShowNum(68,1,(u8)((int)(pid_in_Roll.ki * (float)10)%(int)10),1,6);
//						OLED_ShowNum(68,2,(u8)((int)(pid_in_Roll.kd * (float)10)%(int)10),1,6);
//						OLED_ShowNum(68,3,(u8)((int)(pid_out_Roll.kp * (float)10)%(int)10),1,6);
//						OLED_ShowNum(76,0,(u8)((int)(pid_in_Roll.kp * (float)100)%(int)10),1,6);
//						OLED_ShowNum(76,1,(u8)((int)(pid_in_Roll.ki * (float)100)%(int)10),1,6);
//						OLED_ShowNum(76,2,(u8)((int)(pid_in_Roll.kd * (float)100)%(int)10),1,6);
//						OLED_ShowNum(76,3,(u8)((int)(pid_out_Roll.kp * (float)100)%(int)10),1,6);
//						
					}
		}
		
}


