#ifndef _STRUCTCONFIG_H
#define _STRUCTCONFIG_H
#include "stdint.h"

#define BYTE0(dwTemp)       ( *( (char *)(&dwTemp)		) )
#define BYTE1(dwTemp)       ( *( (char *)(&dwTemp) + 1) )
#define BYTE2(dwTemp)       ( *( (char *)(&dwTemp) + 2) )
#define BYTE3(dwTemp)       ( *( (char *)(&dwTemp) + 3) )


//三轴整型（MPU6050原始数据）
typedef struct
{
	int16_t X;
	int16_t Y;
	int16_t Z;
}INT16_XYZ;

//三轴short型（MPU6050原始数据）
typedef struct
{
	short X;
	short Y;
	short Z;
}SHORT_XYZ;

//三轴浮点型
typedef struct
{
	float X;
	float Y;
	float Z;
}FLOAT_XYZ;

//姿态解算后的角度
typedef struct
{
	float rol;
	float pit;
	float yaw;
}FLOAT_ANGLE;

//遥控器的数据结构 
typedef struct
{
	int16_t ROLL;
	int16_t PITCH;
	int16_t THROTTLE;
	int16_t YAW;
}RC_TYPE;


typedef struct
{
  float desired;      //< 被调量期望值
  float error;        //< 期望值-实际值
  float prevError;    //< 前一次偏差
  float integ;        //< 积分部分
  float deriv;        //< 微分部分
  float kp;           //< 比例参数
  float ki;           //< 积分参数
  float kd;           //< 微分参数
  float outP;         //< pid比例部分，调试用
  float outI;         //< pid积分部分，调试用
  float outD;         //< pid微分部分，调试用
  float iLimit;       //< 积分限制
} pidsuite;





extern pidsuite pid_out_Pitch;			//俯仰角外环pid
extern pidsuite pid_out_Roll;			//横滚角外环pid
extern pidsuite pid_in_Pitch;			//俯仰角内环pid
extern pidsuite pid_in_Roll;				//横滚角内环pid
extern pidsuite pid_in_Yaw;				//偏航角内环pid

extern uint8_t LED_Scan ;
extern uint8_t IMU_Scan ;
extern uint8_t IRQ_Scan ;
extern uint8_t Batt_Scan;


//传感器参数
extern FLOAT_XYZ 	 Gyr_degree,Gyr_rad;	              //把陀螺仪的各通道读出的数据，转换成弧度制 和 度
extern INT16_XYZ	 GYRO_OFFSET_RAW,ACC_OFFSET_RAW;		 //零漂数据
extern INT16_XYZ	 MPU6050_ACC_RAW,MPU6050_GYRO_RAW;	     //读取值原始数据
extern FLOAT_XYZ 	 Acc_filt,Gry_filt;	                  //滤波后的各通道数据
extern float       	 DCMgb[3][3];
extern float       	 Zacc,accb[3];





//extern float pitch,roll,yaw; 		//欧拉角
////extern short aacx,aacy,aacz;		//加速度传感器原始数据
//extern short gyrox,gyroy,gyroz;	//陀螺仪原始数据	
extern FLOAT_ANGLE Att_Angle;//飞机姿态数据
extern SHORT_XYZ Gyr,Acc;; //加速度和陀螺仪数据
extern short temp;

extern FLOAT_ANGLE Measure_Angle,Target_Angle;
extern uint8_t connected_flag;	//正常连接标志位
extern RC_TYPE RC_Control;//遥控器数据
#endif







