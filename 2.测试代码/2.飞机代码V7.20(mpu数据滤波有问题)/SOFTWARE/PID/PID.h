#ifndef _PID
#define _PID
#include "structconfig.h"
#include "stdint.h"
#define DEFAULT_PID_INTEGRATION_LIMIT  100.0
#define YM_Dead 100

//************外环pid参数**************************//
#define PID_OUT_PITCH_KP  2.4 //2.5
#define PID_OUT_PITCH_KI  0   //0
#define PID_OUT_PITCH_KD  0   //0
#define PID_OUT_PITCH_INTEGRATION_LIMIT   500.0 // 500.0

#define PID_OUT_ROLL_KP  2.49    //3
#define PID_OUT_ROLL_KI  0    //0
#define PID_OUT_ROLL_KD  0    //0
#define PID_OUT_ROLL_INTEGRATION_LIMIT    500.0 // 500.0


//************内环pid参数**************************//
#define PID_IN_PITCH_KP  2.60     //0.5
#define PID_IN_PITCH_KI  0.01   //0.003
#define PID_IN_PITCH_KD  2.44    //0.25
#define PID_IN_PITCH_INTEGRATION_LIMIT   500.0 // 500.0

#define PID_IN_ROLL_KP  2.10      //0.3
#define PID_IN_ROLL_KI  0.01    //0.003
#define PID_IN_ROLL_KD  1.25     //0.20
#define PID_IN_ROLL_INTEGRATION_LIMIT    500.0 // 500.0

#define PID_IN_YAW_KP   1        //3.0
#define PID_IN_YAW_KI   0        //0
#define PID_IN_YAW_KD   0        //0
#define PID_IN_YAW_INTEGRATION_LIMIT   200.0 // 200.0



void PID_controllerInit(void);     //pid参数初始化
float PID_Postion_Cal(pidsuite* pid, const float measured,float expect);//PID函数

#endif
