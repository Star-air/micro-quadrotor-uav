#include "sys.h"

u16 MOTO1_PWM,MOTO2_PWM,MOTO3_PWM,MOTO4_PWM;    //四电机pwm

pidsuite pid_out_Pitch;			//俯仰角外环pid
pidsuite pid_out_Roll;			//横滚角外环pid
pidsuite pid_in_Pitch;			//俯仰角内环pid
pidsuite pid_in_Roll;				//横滚角内环pid
pidsuite pid_in_Yaw;				//偏航角内环pid

float pid_out_pitch=0;//外环pitch
float pid_out_roll=0;//外环roll
float pid_in_pitch=0;//内环pitch
float pid_in_roll=0;//内环roll
float pid_in_yaw=0;//内环yaw

float pitch,roll,yaw; 		//欧拉角
short gyrox,gyroy,gyroz;	//陀螺仪原始数据	
short aacx,aacy,aacz;		//加速度传感器原始数据

FLOAT_ANGLE Measure_Angle,Target_Angle;


void Control(uint8_t armed)
{
	
			
			Measure_Angle.pit=Att_Angle.pit;//测量角度
			Measure_Angle.rol=Att_Angle.rol;//测量角度
			Measure_Angle.yaw=Att_Angle.yaw;//测量角度
			Target_Angle.pit=(float)(-(RC_Control.PITCH-250)/10.0f);
			Target_Angle.rol=(float)(-(RC_Control.ROLL-250)/10.0f);
			Target_Angle.yaw=(float)((RC_Control.YAW-1800)/10.0f);
//			printf("pit=%0.2f rol=%0.2f yaw=%0.2f\r\n",Target_Angle.pit,Target_Angle.rol,Target_Angle.yaw);
			pid_out_pitch = PID_Postion_Cal(&pid_out_Pitch,Measure_Angle.pit,Target_Angle.pit);//PIT外环角度环
			pid_in_pitch=PID_Postion_Cal(&pid_in_Pitch,Gyr_rad.Y,pid_out_pitch);//PIT内环速度环

			pid_out_roll = PID_Postion_Cal(&pid_out_Roll,Measure_Angle.rol,Target_Angle.rol);//ROLL外环角度环
			pid_in_roll=PID_Postion_Cal(&pid_in_Roll,Gyr_rad.X,pid_out_roll);//ROLL内环速度环
		
			pid_in_yaw=PID_Postion_Cal(&pid_in_Yaw,Gyr_rad.Z,0);
			if((RC_Control.THROTTLE>YM_Dead) && armed)        //油门大于死区才进行控制
			{
					//动力分配
					MOTO1_PWM=RC_Control.THROTTLE-pid_in_pitch+pid_in_roll-pid_in_yaw;//-pid_in_pitchSafety_Check
					MOTO2_PWM=RC_Control.THROTTLE-pid_in_pitch-pid_in_roll+pid_in_yaw;//-pid_in_pitch
					MOTO3_PWM=RC_Control.THROTTLE+pid_in_pitch-pid_in_roll-pid_in_yaw;//+pid_in_pitch
					MOTO4_PWM=RC_Control.THROTTLE+pid_in_pitch+pid_in_roll+pid_in_yaw;//+pid_in_pitch
			}
			else
			{
					MOTO1_PWM=0;
					MOTO2_PWM=0;
					MOTO3_PWM=0;
					MOTO4_PWM=0;
					pid_in_Pitch.integ=0;//积分清零
					pid_in_Roll.integ=0;
					pid_in_Yaw.integ=0;
			}
		
			Moto_Pwm(MOTO1_PWM,MOTO2_PWM,MOTO3_PWM,MOTO4_PWM);
			Safety_Check();
}


/******************************************************************************************
* 函  数：void Safety_Check(void)
* 功  能：飞机姿态安全监测
* 参  数：无
* 返回值：无
* 备  注：如果飞机角度和加速度异常就将飞机上锁并停止电机，防止电机狂转打坏桨叶
*******************************************************************************************/
void Safety_Check(void)
{
  if((fabs(Att_Angle.pit)>45.0f||fabs(Att_Angle.rol)>45.0f) && (fabs(Acc.X)>9.0f||fabs(Acc.Y)>9.0f))
	{
		
			MOTO1_PWM = 0;
			MOTO2_PWM = 0;
			MOTO3_PWM = 0;
			MOTO4_PWM = 0;
			Moto_Pwm(MOTO1_PWM,MOTO2_PWM,MOTO3_PWM,MOTO4_PWM);
	}
}



