
/*
 * Auto generated Run-Time-Environment Component Configuration File
 *      *** Do not modify ! ***
 *
 * Project: 'Template' 
 * Target:  'Template' 
 */

#ifndef RTE_COMPONENTS_H
#define RTE_COMPONENTS_H

#define RTE_DEVICE_STDPERIPH_ADC
#define RTE_DEVICE_STDPERIPH_BKP
#define RTE_DEVICE_STDPERIPH_CAN
#define RTE_DEVICE_STDPERIPH_CEC

#endif /* RTE_COMPONENTS_H */
