#ifndef __MENU_UI_H
#define __MENU_UI_H



/*
    多级菜单结构体
 */
typedef struct
{
    unsigned char current;
    unsigned char down;//向下，单击
    unsigned char enter;//进入或保存(最后一级),双击
    unsigned char back;//退出，长按
    void (*current_operation)();
}key_table;




void showUI(unsigned char keycode);




#endif



