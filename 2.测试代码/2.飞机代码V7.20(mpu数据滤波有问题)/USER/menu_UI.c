#include "sys.h"

#define CLOSEUI 8//飞行模式界面
unsigned char func_index=0;//当前函数索引号
void (*current_operation_index)();//函数指针
void closeUI(void);//关闭UI，准备起飞
void fun0(void);
void FirstFloor1_power(void);
void FirstFloor1_posture(void);
void FirstFloor1_pid(void);
void FirstFloor1_remote(void);
void power(void);
void posture(void);
void pid(void);
key_table table[10] =
{
	{0,0,1,CLOSEUI,(*fun0)},//主界面，显示logo介绍 向上向下无效，进入-第一层,退出-进入起飞模式(关闭OLED刷新),起飞模式放最后一个函数
	
	{1,2,5,0,(*FirstFloor1_power)},//第一层1 [电量显示] 姿态信息校正 PID调节 遥控器校对 进入-电量显示界面，退出-主界面
	{2,3,6,0,(*FirstFloor1_posture)},//第一层2 电量显示 [姿态信息校正] PID调节 遥控器校对 进入-姿态信息矫正界面，退出-主界面
	{3,4,7,0,(*FirstFloor1_pid)},//第一层3 电量显示 姿态信息校正 [PID调节] 遥控器校对 进入-PID调节界面，退出-主界面
	{4,1,0,0,(*FirstFloor1_remote)},//第一层4 电量显示 姿态信息校正 PID调节 [遥控器校对] 进入-遥控器矫正界面，退出-主界面
	
	{5,5,5,1,(*power)},//电量显示 向下(复用)-无用  进入-无用，退出-第一层1
	{6,6,6,2,(*posture)},//姿态数据显示和发送 向下(复用)-无用  进入-无用，退出-第一层2 
	{7,7,7,3,(*pid)},//姿态数据显示和发送 向下(复用)-无用  进入-无用，退出-第一层2 
	{CLOSEUI,CLOSEUI,CLOSEUI,CLOSEUI,(*closeUI)}//准备起飞
};


void closeUI(void)//关闭UI，准备起飞
{
		//UI显示标志位清0
		//关掉按键相关定时器和中断
		TIM_ITConfig(TIM3,TIM_IT_Update,DISABLE ); //关闭指定的TIM3中断
		TIM_Cmd(TIM3, DISABLE);  //TIM3				
		TIM3->CR1&=0xfe;    //关闭定时器3
		MOTOR_Init();
		//打开任务时基定时器
		TIM_ITConfig(TIM1,TIM_IT_Update,ENABLE);
		TIM_Cmd(TIM1,ENABLE);
		
		//覆盖中断设置
		Exit_Init();//IRQ触发中断
		//起飞标志位置1
		//检测油门
		OLED_ShowString(0,0,"discharge OLED",6);
		OLED_ShowString(0,1,"to Fly",6);
		
		UI_flag = 0;//关闭UI显示
		Fly_flag = 1;//打开飞行模式
		OLED_ShowNum(0,2,UI_flag,1,6);
		OLED_ShowNum(0,3,Fly_flag,1,6);
		OLED_Clear();//清屏
		
		//否则显示提示，将油门关掉
		
}


void fun0(void)
{
		float V;
		
		//UI_Refresh = 0;//不需要刷
		static u8 flag=1;
		UI_Refresh = 1;//需要刷
		if(flag==1)
		{
				flag=0;
				OLED_Clear();//清屏
				OLED_ShowString(0,0,"welcome to",6);
				OLED_ShowString(0,1,"FlySystem",6);
				OLED_ShowString(0,4,"vol:",6);
		}
		//OLED_Clear();//清屏
		//OLED_ShowString(0,0,"welcome to",6);
		//OLED_ShowString(0,1,"FlySystem",6);
		//OLED_ShowNum(0,2,UI_flag,1,6);
		//OLED_ShowNum(0,3,Fly_flag,1,6);
		
		V=BATT_GetVoltage()+0.35f;
		OLED_ShowNum(52,4,(u8)((int)V%(int)10),1,6);
		OLED_ShowString(60,4,".",6);
		OLED_ShowNum(68,4,(u8)((int)(V * (float)10)%(int)10),1,6);
		OLED_ShowNum(76,4,(u8)((int)(V * (float)100)%(int)10),1,6);
}

void FirstFloor1_power(void)
{
		UI_Refresh = 0;//不需要刷
		OLED_Clear();//清屏
		OLED_ShowString(0,0,"[power]",6);
		OLED_ShowString(0,1," posture ",6);
		OLED_ShowString(0,2," pid ",6);
		OLED_ShowString(0,3," remote ",6);
}

void FirstFloor1_posture(void)
{
		UI_Refresh = 0;//不需要刷
		OLED_Clear();//清屏
		OLED_ShowString(0,0," power ",6);
		OLED_ShowString(0,1,"[posture]",6);
		OLED_ShowString(0,2," pid ",6);
		OLED_ShowString(0,3," remote ",6);
}

void FirstFloor1_pid(void)
{
		UI_Refresh = 0;//不需要刷
		OLED_Clear();//清屏
		OLED_ShowString(0,0," power ",6);
		OLED_ShowString(0,1," posture ",6);
		OLED_ShowString(0,2,"[pid]",6);
		OLED_ShowString(0,3," remote ",6);
}

void FirstFloor1_remote(void)
{
		UI_Refresh = 0;//不需要刷
		OLED_Clear();//清屏
		OLED_ShowString(0,0," power ",6);
		OLED_ShowString(0,1," posture ",6);
		OLED_ShowString(0,2," pid ",6);
		OLED_ShowString(0,3,"[remote]",6);
}


void power(void)//显示电量
{
		UI_Refresh = 1;//需要刷
		//读取ADC
		//显示 只写入会变的部分，不清屏
}

void posture(void)//姿态数据显示和发送
{
		
		
	  static u8 flag=1;
		UI_Refresh = 1;//需要刷
		if(flag==1)
		{
				flag=0;
				OLED_Clear();//清屏
				OLED_ShowString(0,0,"pit:",16);
				OLED_ShowString(0,2,"rol:",16);
				OLED_ShowString(0,4,"yaw:",16);
		}
		//读取姿态数据
		Prepare_Data();
		if(Att_Angle.pit>=0)
		{
				OLED_ShowString(36,0," ",16);
				OLED_ShowNum(48,0,(u8)Att_Angle.pit,5,16);
		}
		else
		{
				OLED_ShowString(36,0,"-",16);
				OLED_ShowNum(48,0,(u8)-Att_Angle.pit,5,16);
		}
		if(Att_Angle.rol>=0)
		{
				OLED_ShowString(36,2," ",16);
				OLED_ShowNum(48,2,(u8)Att_Angle.rol,5,16);
		}
		else
		{
				OLED_ShowString(36,2,"-",16);
				OLED_ShowNum(48,2,(u8)-Att_Angle.rol,5,16);
		}
		if(Att_Angle.yaw>=0)
		{
				OLED_ShowString(36,4," ",16);
				OLED_ShowNum(48,4,(u8)Att_Angle.yaw,5,16);
		}
		else
		{
				OLED_ShowString(36,4,"-",16);
				OLED_ShowNum(48,4,(u8)-Att_Angle.yaw,5,16);
		}
			
	
		//发送到上位机
		//显示
}


void pid(void)
{
		static u8 flag=1;
		UI_Refresh = 1;//需要刷
		if(flag==1)
		{
				flag=0;
				OLED_Clear();//清屏
				OLED_ShowString(0,0,"po_kp:",16);
				OLED_ShowString(0,2,"po_ki:",16);
				OLED_ShowString(0,4,"po_kd:",16);
		}
		OLED_ShowNum(52,0,(u8)(pid_out_Pitch.kp),1,16);
		OLED_ShowNum(52,2,(u8)(pid_out_Pitch.ki),1,16);
		OLED_ShowNum(52,4,(u8)(pid_out_Pitch.kd),1,16);
		OLED_ShowString(60,0,".",16);
		OLED_ShowString(60,2,".",16);
		OLED_ShowString(60,4,".",16);
		OLED_ShowNum(68,0,(u8)((int)(pid_out_Pitch.kp * (float)10)%(int)10),1,16);
		OLED_ShowNum(68,2,(u8)((int)(pid_out_Pitch.ki * (float)10)%(int)10),1,16);
		OLED_ShowNum(68,4,(u8)((int)(pid_out_Pitch.kd * (float)10)%(int)10),1,16);
		
}

/*
    菜单页面数组
		当前页面,向上显示页面，向下显示页面，进入页面，退出页面
 */


void showUI(unsigned char keycode)
{
		switch(keycode)
		{
				case 0:func_index=table[func_index].current;break;
				case 1:func_index=table[func_index].down;break;
				case 2:func_index=table[func_index].enter;break;
				case 3:func_index=table[func_index].back;break;
				default:break;
		}
		//执行函数
		current_operation_index=table[func_index].current_operation;//获得函数地址
		(*current_operation_index)();//执行函数
		
}







