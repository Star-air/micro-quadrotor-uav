#ifndef   _SPI_H
#define   _SPI_H

#include "stdint.h"

void SPI2_Init(void);
void SPI2_SetSpeed(uint8_t SpeedSet);
uint8_t SPI2_ReadWriteByte(uint8_t TxData);


#endif

