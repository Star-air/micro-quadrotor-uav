#include "sys.h"


#define KEY_GPIO_PortSource		GPIO_PortSourceGPIOA
#define KEY_GPIO_PinSource		GPIO_PinSource2
#define KEY_GPIO_PORT		GPIOA
#define KEY_GPIO_PIN		GPIO_Pin_2
#define KEY_EXTI_LINE 		EXTI_Line2

myKey_ValueTypedef key_Value;//按键结构体
//5ms计数器
unsigned short delay_Time5ms = 0;
//按键被按下标志
unsigned char myKey_IsPressed_Flag = 0;
//值被改变标志
unsigned char myKey_ValueChangedFlag = 0;
//按键扫描延时周期
unsigned char myKey_GetKeyValue_delayTime5ms;
//双击按下次数
unsigned char mykey_Doubletimes= 0;
//两次按键间隔时间
unsigned char mykey_Double_delaytimes= 0;
unsigned char delay_EXTI2_delayTime5ms;

void TIM3_Int_Init(u16 arr,u16 psc);//定时器3初始化

void Key_Init(void)
{
	
		GPIO_InitTypeDef GPIO_InitStructure;
		EXTI_InitTypeDef EXTI_InitStruct;
		NVIC_InitTypeDef NVIC_InitStructure;
		//时钟配置
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
		/* EXTI的时钟要设置AFIO寄存器 */
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE) ;
		//GPIO配置
		GPIO_InitStructure.GPIO_Pin =  KEY_GPIO_PIN;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;//上拉输入
		GPIO_Init(KEY_GPIO_PORT, &GPIO_InitStructure);
		
		/* 初始化EXTI外设 */
		/* 选择作为EXTI线的GPIO引脚 */
		GPIO_EXTILineConfig( KEY_GPIO_PortSource , KEY_GPIO_PinSource) ;
		/* 配置中断or事件线 */
		EXTI_InitStruct.EXTI_Line = KEY_EXTI_LINE ;
		/* 配置模式：中断or事件 */
		EXTI_InitStruct.EXTI_Mode = EXTI_Mode_Interrupt ;
		/* 配置边沿触发 上升or下降 */
		EXTI_InitStruct.EXTI_Trigger = EXTI_Trigger_Falling ;
		/* 使能EXTI线 */
		EXTI_InitStruct.EXTI_LineCmd = ENABLE ;
		EXTI_Init(&EXTI_InitStruct) ;

		NVIC_InitStructure.NVIC_IRQChannel = EXTI2_IRQn;  //
		NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 3;  //先占优先级2级
		NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;  //从优先级2级
		NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE; 
		NVIC_Init(&NVIC_InitStructure);  //初始化NVIC寄存器


		//中断的时间为t：t = (arr * psc / APB1*2) * 1000 ms
		TIM3_Int_Init(1000,360); //5ms
}

void EXTI2_IRQHandler()
{
			
		if(UI_flag)
		{
				delay_EXTI2_delayTime5ms = 0;
				while(delay_EXTI2_delayTime5ms > 2);
				if(!GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_2))
				{
					myKey_IsPressed_Flag = 1;
				}
				EXTI_ClearITPendingBit(KEY_EXTI_LINE);
		}
		else if(Fly_flag)
		{
				uint8_t sta;
				if(EXTI_GetITStatus(EXTI_Line2) != RESET )
				{
					 NRF24L01_CE=0;//拉低CE，以便读取NRF中STATUS中的数据
					 sta=NRF24L01_Read_Reg(STATUS); //读取STATUS中的数据，以便判断是由什么中断源触发的IRQ中断
					/* 发送完成中断 TX_OK */
					if(sta & TX_OK)                                   
					{										
						NRF24L01_RX_Mode();	//切换到接收模式
						NRF24L01_Write_Reg(NRF_WRITE_REG+STATUS,TX_OK); //清除TX_DS或MAX_RT中断标志
						NRF24L01_Write_Reg(FLUSH_TX,0xff);//清除TX FIFO寄存器 
						//printf("Sent OK!!!!\r\n");
					}
					/* 接收完成中断 RX_OK */
					if(sta & RX_OK) 
					{	
						NRF24L01_RxPackets(NRF24L01_RX_DATA);	
						Remote_Data_ReceiveAnalysis();
						NRF24L01_Write_Reg(NRF_WRITE_REG+STATUS,RX_OK); //清除发送完成标志
						
						//printf("Receive OK!!!!\r\n");	
					}
					/* 达到最大重发次数中断  MAX_TX */
					if(sta & MAX_TX)                                  
					{											
						NRF24L01_TX_Mode();	//切换到发送模式
						NRF24L01_Write_Reg(NRF_WRITE_REG+STATUS,MAX_TX);//清除接达到最大重发标志
						NRF24L01_Write_Reg(FLUSH_TX,0xff);//清除TX_FIFO
						//printf("Sent Max Data!!!\r\n"); 
					}
					EXTI_ClearITPendingBit(EXTI_Line2);
				}				
		}
		else
		{
				
		}
		
	

}


void judge_KeyValue(GPIO_TypeDef * keyGPIO, uint16_t keyGPIO_Pin, myKey_ValueTypedef * keyValue)
{

	//键处理
	if(!GPIO_ReadInputDataBit(keyGPIO, keyGPIO_Pin))//如果按键处于按下状态
	{
		if(keyValue->keyState == KEY_UNPRESSED)
		{
			keyValue->keyState = KEY_PRESSED;
			keyValue->pressedTime5ms = 0;
		}
		else if(keyValue->keyState == KEY_PRESSED && keyValue->pressedTime5ms >= 200)
		{
			keyValue->isLongPressing = 1;	
		}
	}
	//如果按键处于弹起状态状态并且之前有被按下，则将根据按下时间确定键值
	else if(GPIO_ReadInputDataBit(keyGPIO, keyGPIO_Pin) && keyValue->keyState == KEY_PRESSED)
	{
			keyValue->keyState = KEY_UNPRESSED;
			keyValue->isLongPressing = 0;	
			if(keyValue->pressedTime5ms >= 250)//超过2s 
			{
				keyValue->longPressed = 1;
				keyValue->shortPressed = 0;
				keyValue->doublePressed = 0;
			}
			else//没有超过2s 不能确定是不是单击,开始计时这一次触发到下一次触发的时间，如果再200ms以内，就是双击，如果再200ms以外就是单击
			{
					keyValue->longPressed = 0;//肯定不是长按
					mykey_Doubletimes++;//记录短按次数
					if(mykey_Doubletimes==1)
					{
							mykey_Double_delaytimes=0;//开始计时
					}
					if(mykey_Doubletimes==2)//如果按下两次了,肯定再200ms以内的
					{
							keyValue->doublePressed = 1;//是双击
							keyValue->shortPressed = 0;//不是短键
							mykey_Doubletimes = 0;//次数清零
							myKey_IsPressed_Flag = 0;	
					}
			}
	
	}

}

void myKey_GetKeyValue(void)
{
	if(myKey_IsPressed_Flag == 1 && myKey_GetKeyValue_delayTime5ms > 10) //判断如果有按键被按下了 ，并且按键扫描周期到达相应时间了则开始扫描按键
	{
		
		
		judge_KeyValue(KEY_GPIO_PORT, KEY_GPIO_PIN, &key_Value);
		myKey_GetKeyValue_delayTime5ms = 0;//清楚按键扫描周期计数
		
	}
	
}


//按键计时中断
//定时器3中断服务程序 5ms一次
void TIM3_IRQHandler(void)   //TIM3中断
{
	if (TIM_GetITStatus(TIM3, TIM_IT_Update) != RESET)  //检查TIM3更新中断发生与否
		{
			TIM_ClearITPendingBit(TIM3, TIM_IT_Update  );  //清除TIMx更新中断标志 
			
			delay_Time5ms++;
			if(delay_Time5ms == 200)//1S
			{
				delay_Time5ms = 0;
				//testDynamicNum ++;
			}
			
			if(key_Value.keyState == KEY_PRESSED)
			{
				key_Value.pressedTime5ms++;
			}
			
			
			delay_EXTI2_delayTime5ms++;
			myKey_GetKeyValue_delayTime5ms++;
			mykey_Double_delaytimes++;
			if((mykey_Doubletimes==1)&&(mykey_Double_delaytimes>=50))//如果已经超过100ms还没等到第二次，说明是短
			{
					key_Value.longPressed = 0;//肯定不是长按
					key_Value.shortPressed = 1;
					key_Value.doublePressed = 0;//不是双击
					mykey_Doubletimes=0;//按下次数清0
					myKey_IsPressed_Flag = 0;	
			}	
//			if(delay_EXTI0_delayTime5ms > 200)delay_EXTI0_delayTime5ms = 0;
//			if(delay_EXTI15_10_delayTime5ms > 200)delay_EXTI15_10_delayTime5ms = 0;
//			if(myKey_GetKeyValue_delayTime5ms > 200)myKey_GetKeyValue_delayTime5ms = 0;
		}
}



//这里时钟选择为APB1的2倍，而APB1为36M
//arr：自动重装值。
//psc：时钟预分频数
//一次中断的时间为t：t = (arr * psc / APB1*2) * 1000 ms
void TIM3_Int_Init(u16 arr,u16 psc)
{
  TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
 
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE); //时钟使能
	
	//定时器TIM3初始化
	TIM_TimeBaseStructure.TIM_Period = arr; //设置在下一个更新事件装入活动的自动重装载寄存器周期的值	
	TIM_TimeBaseStructure.TIM_Prescaler =psc; //设置用来作为TIMx时钟频率除数的预分频值
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1; //设置时钟分割:TDTS = Tck_tim
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;  //TIM向上计数模式
	TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure); //根据指定的参数初始化TIMx的时间基数单位
 
	TIM_ITConfig(TIM3,TIM_IT_Update,ENABLE ); //使能指定的TIM3中断,允许更新中断
 
	//中断优先级NVIC设置
	NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;  //TIM3中断
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;  //先占优先级0级
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;  //从优先级0级
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE; //IRQ通道被使能
	NVIC_Init(&NVIC_InitStructure);  //初始化NVIC寄存器
 
 
	TIM_Cmd(TIM3, ENABLE);  //使能TIM3				
	
}

