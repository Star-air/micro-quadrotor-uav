#include "sys.h"




uint8_t DataID;	//数据包ID
uint8_t connected_flag=0;	//数据包ID
RC_TYPE RC_Control;


void Recdata(void)
{
			
			NRF24L01_RX_Mode();	//切换到接收模式
			NRF24L01_RxPacket(NRF24L01_RX_DATA);	

}

void Remote_Data_ReceiveAnalysis(void)
{
			if(NRF24L01_RX_DATA[0]==0xFF)//帧头
			{
					if(NRF24L01_RX_DATA[1]==0x00)//校验位
					{
							RC_Control.THROTTLE      = (NRF24L01_RX_DATA[2]<<8|NRF24L01_RX_DATA[3])*1000/4096;		//油门
							RC_Control.YAW = NRF24L01_RX_DATA[4]<<8|NRF24L01_RX_DATA[5]; 		//偏航
							RC_Control.PITCH     = NRF24L01_RX_DATA[6]<<8|NRF24L01_RX_DATA[7];	  	//俯仰
							RC_Control.ROLL    = NRF24L01_RX_DATA[8]<<8|NRF24L01_RX_DATA[9];		//翻滚
							DataID = NRF24L01_RX_DATA[10];
					}
			}
}


/**************************************************************************************************************
* 函  数：void UnControl_Land(void)
* 功  能：信号中断紧急降落
* 参  数：无
* 返回值：无
* 备  注：粗略处理，有待完善
***************************************************************************************************************/
void UnControl_Land(void)       
{
	RC_Control.THROTTLE -= 20;
	if(RC_Control.THROTTLE <= 100)
		RC_Control.THROTTLE = 100;
}

/**************************************************************************************************************
* 函  数：void SI24R1_SingalCheck(void)
* 功  能：信号中断检测
* 参  数：无
* 返回值：无
* 备  注：如果飞机处于解锁状态但是,当前数据包的ID等于前一个数据包的ID，这就说明遥控器与飞机断开连接
***************************************************************************************************************/
void SingalCheck(void)
{
		static uint8_t PreDataID = 250; 
	
		if(DataID == PreDataID)//飞机与遥控断开连接
		{
				
				UnControl_Land(); //紧急降落处理	
				connected_flag = 0;
		}
		else
		{
				connected_flag = 1;
		}
		PreDataID = DataID;

}


/**************************************************************************************************************
* 函  数：void SendToRemote(void)
* 功  能：飞机状态数据发送给遥控器
* 参  数：无
* 返回值：无
* 备  注：注意：SI24R1单次发送最大32个字节，请勿越界
***************************************************************************************************************/
void SendToRemote(void)
{
	int16_t temp; 	

	NRF24L01_TX_DATA[0] = 0xFF;//帧头
	
	NRF24L01_TX_DATA[1] = 0x00; //标志位组
	
	temp = (u16)RC_Control.THROTTLE; //油门
	NRF24L01_TX_DATA[2] = BYTE1(temp);
	NRF24L01_TX_DATA[3] = BYTE0(temp);
	temp = (int)(Att_Angle.yaw*100); //航向
	NRF24L01_TX_DATA[4] = BYTE1(temp);
	NRF24L01_TX_DATA[5] = BYTE0(temp);
	temp = (int)(Att_Angle.pit*100); //俯仰
	NRF24L01_TX_DATA[6] = BYTE1(temp);
	NRF24L01_TX_DATA[7] = BYTE0(temp);
	temp = (int)(Att_Angle.rol*100); //横滚
	NRF24L01_TX_DATA[8] = BYTE1(temp);
	NRF24L01_TX_DATA[9] = BYTE0(temp);

	NRF24L01_TxPackets(NRF24L01_TX_DATA); //发送函数
}

