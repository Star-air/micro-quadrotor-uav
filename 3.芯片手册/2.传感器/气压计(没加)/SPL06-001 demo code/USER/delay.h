#ifndef _DELAY_H
#define _DELAY_H 


#include "sys.h"


extern void cycleCounterInit(void);
extern void delay_ms(uint32_t nTime);
extern uint32_t GetSysTime_us(void); 
extern void delay_us(unsigned int i);

#endif
