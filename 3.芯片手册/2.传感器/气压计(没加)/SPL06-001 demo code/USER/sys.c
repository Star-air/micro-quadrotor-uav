/*******************************************************************
 *系统主控函数
 *@brief 
 *@brief 
 *@time  2016.1.8
 *@editor小南&zin
 *飞控爱好QQ群551883670,邮箱759421287@qq.com
 *非授权使用人员，禁止使用。禁止传阅，违者一经发现，侵权处理。
 ******************************************************************/
 #include "stm32f10x.h"
#include "sys.h"
#include "I2C.h"
#include "USART.h"
#include  "TIM.h"
#include  "SPL06_001.h"
/******************************************************************/

/***********************************************************************
 * 
 * @param[in] 
 * @param[out] 
 * @return     
 **********************************************************************/
void _g_Init_sys(void)
{
	I2c_Soft_Init();

	spl0601_init();	

	TIM3_Config(); 
}

/***********************************************************************
 * 
 * @param[in] 
 * @param[out] 
 * @return     
 **********************************************************************/
void TIM3_IRQHandler(void)   //TIM3中断
{
	if (TIM_GetITStatus(TIM3, TIM_IT_Update) != RESET) //检查指定的TIM中断发生与否:TIM 中断源 
	{	
	    static uint8_t cnt_12ms = 0;
      
		  cnt_12ms++;
		  if(cnt_12ms>4)
			{
					cnt_12ms = 0;
			    user_spl0601_get();
			}
	}
	TIM_ClearITPendingBit(TIM3, TIM_IT_Update  );  //清除TIMx的中断待处理位:TIM 中断源
}

/********************************END OF FILE*****************************************/


