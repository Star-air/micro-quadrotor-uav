#ifndef __SYS_H
#define __SYS_H

//#pragma pack(push,4) 
//#pragma pack(4) 
//__unaligned
//__packed
//__align(4)
//==================================================================================
//User data type define.
//==================================================================================

//----------------------------int type----------------------------------------------
#define   __I     volatile const       /*!< Defines 'read only' permissions                 */
#define     __O     volatile             /*!< Defines 'write only' permissions                */
#define     __IO    volatile             /*!< Defines 'read / write' permissions              */
#define NULL 0
typedef   signed          char int8_t;
typedef   signed short     int int16_t;
typedef   signed 	     int int32_t;
typedef   signed       long long int64_t;

typedef unsigned           char uint8_t;
typedef unsigned short     int uint16_t;
typedef unsigned       int uint32_t;
typedef unsigned      long long uint64_t;


typedef __IO int32_t vint32_t;
typedef __IO int16_t vint16_t;
typedef __IO int8_t  vint8_t;
typedef __IO uint32_t vuint32_t;
typedef __IO uint16_t vuint16_t;
typedef __IO uint8_t  vuint8_t;



#include "delay.h"      //system delay,common.


extern volatile uint32_t sysTickUptime;
extern void _g_Init_sys(void);

#endif

/******************************END OF FILE *******************************************/

