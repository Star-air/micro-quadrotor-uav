/* Includes ------------------------------------------------------------------*/
#include "stm32f10x.h"
#include "BSP/BSP.H"
#include "app/uart/uart1.h"
#include "app/rc/rc.h"

#define CLI()      __set_PRIMASK(1)  
#define SEI()      __set_PRIMASK(0)
////////////////////////////////////////////////////////////////////////////////
void SYS_INIT(void)
{
	MCO_INIT();
	LED_INIT();
	LED_FLASH();
	Tim3_Init(500);				//1000=1MS,500=0.5MS
	Moto_Init();
//	Uart1_Init(500000);	
	Spi1_Init();
	Nvic_Init();					//中断初始化
	Nrf24l01_Init(MODEL_TX2,40);
	if(Nrf24l01_Check())	Uart1_Put_String("NRF24L01 IS OK !\r\n");
	else 									Uart1_Put_String("NRF24L01 IS NOT OK !\r\n");
	ANOTech_taobao_com_I2C1_INIT(0xA6,400000,2,1,1,1,1);
	MPU6050_Init();
	ADC1_Init();
	FLASH_Unlock();
	EE_INIT();
	EE_READ_ACC_OFFSET();
	EE_READ_GYRO_OFFSET();
	EE_READ_PID();
}
////////////////////////////////////////////////////////////////////////////////
int main(void)
{
	SYS_INIT_OK=0;
	SYS_INIT();
	SYS_INIT_OK=1;
	while (1)
	{			     
		
	}
}
////////////////////////////////////////////////////////////////////////////////

