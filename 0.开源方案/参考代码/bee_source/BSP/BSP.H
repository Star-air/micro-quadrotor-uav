#ifndef _BSP_BSP_H_
#define _BSP_BSP_H_
#include "tim.h"
#include "usart.h"
#include "spi.h"
#include "nrf24l01.h"
#include "led.h"
#include "ANO_Tech_STM32F10x_I2C.h"
#include "mpu6050.h"
#include "moto.h"
#include "eeprom.h"
#include "stm32f10x_flash.h"

extern u8 SYS_INIT_OK;

void Nvic_Init(void);
void EE_INIT(void);
void EE_SAVE_ACC_OFFSET(void);
void EE_READ_ACC_OFFSET(void);
void EE_SAVE_GYRO_OFFSET(void);
void EE_READ_GYRO_OFFSET(void);
void EE_SAVE_PID(void);
void EE_READ_PID(void);
void EE_READ_PID_Send(void)	;	//从flash中获取 并且发送
#endif
