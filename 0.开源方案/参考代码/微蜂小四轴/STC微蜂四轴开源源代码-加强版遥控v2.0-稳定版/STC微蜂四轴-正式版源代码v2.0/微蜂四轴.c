/*------------------------------------------------------------
此产品是基于STC宏晶公司，在2015年最新开发出一款STC15W4K（IAP15W4K）系列的
8位单片机。此系列在STC12系列的基础上 增加了宽电压和多路硬件PWM功能（15位）。
此条件满足了四轴飞行的基本设计需求，经过本人多方面学习与研究的过程中，
掌握了迷你四轴的飞行原理和制作具体要求设计参数等。

此开源源代程序码-是“微蜂四轴”专用程序，基于STC15W4Kxx设计的STC迷你四轴飞行器。
在本程序中采用全中文段落注解方法，方便入门或者单片机玩家，学习与交流。
想全面了解四轴飞行器的飞行原理和程序代码，可上淘宝网采购此产品做深入学习。
本程序是不断更新的程序源代码，想拿到最新，飞行最稳定版，请关注我们相关宣传。

以下是“微蜂四轴”的程序更能说明：
单片机采用STC15W4K48S4-增强型单片机（IPA需修改EEPROM代码）
工作电压为：5V	晶振频率为：27M	无线通讯模块为：NRF24L01+
本程序中 采用了MCU6050做姿态数据采集加PID算法给硬件4路PWM控制MOS管驱动空心杯电机
注意：此程序只兼容：空心杯716电机与45MM专用正反桨（自己的电机或需修改PID参数）
在后期的程序更新中，会增加硬件代码达到预期飞行功能。敬请关注。

开源程序-广大四轴爱好者都可以进行修改与技术交流。
您如果在此基础上能调出更好的飞行效果，可与设计者联系，有丰厚实物奖励哦。
原创设计者：联系方式QQ：250278069  
STC迷你四轴技术交流群：421109612
淘宝网站：http://shop117773209.taobao.com/shop/view_shop.htm?spm=a1z0e.1.0.0.eqIhKz&mytmenu=mdianpu&utkn=g,meztsobuhe3to1427461542456&user_number_id=669230514&scm=1028.1.1.20001
制作完成时间：2015年9月25日-V0.6版(新版稳定版)
------------------------------------------------------------*/
#include <STC15F2K60S2.h>	//STC15W4K48S4 专用头文件
#include <intrins.h>		//STC特殊命令符号声明
#include <MPU6050.H>		//MPU6050数字陀螺仪
#include <STC15W4KPWM.H>	//单片机所有IO口初始化-PWM口初始化
#include <EEPROM.h>			//STC-EEPROM内部存储
#include <NRF24L01.h>	    //NRF24L01 2.4G无线收发模块
//#include <STC15W4K-ADC.h>	//STC15W4K-ADC	硬件ADC模数转换
#include <IMU.H>			//IMU飞行核心算法
//==================================================//
//  外围引脚定义
//==================================================//
sbit LEDH3=P2^4; //四轴航向灯 1=灭 0=亮
sbit LEDH4=P2^5; //四轴航向灯 1=灭 0=亮
sbit LEDH1=P2^6; //四轴航向灯 1=灭 0=亮
sbit LEDH2=P2^7; //四轴航向灯 1=灭 0=亮
sbit LEDZ1=P2^0; //独立LED状态灯 左灯 
sbit LEDZ2=P5^5; //独立LED状态灯 右灯
sbit KASB =P3^3; //复位引脚	0=进入复位
//==================================================//
//  飞行控制变量
//==================================================//
unsigned char JieSuo;	//断开/连接 解锁变量
unsigned char SSLL;     //通讯状态 变量
unsigned char ShiLian;	//失联变量
unsigned char SLJS;		//失联计数变量
unsigned int YouMen;	//油门变量
unsigned int HangXiang;	//航向变量
unsigned int HengGun;	//横滚变量
unsigned int FuYang;	//俯仰变量
unsigned char  FYHG;	//俯仰横滚变量
unsigned char  GaoDu;	//高度变量
unsigned char  DianYa;	//电压变量
unsigned int ADC1,ADC2; //adc模数转换 10位 结果变量
/*------------------------------------------------
                  全局函数定义
------------------------------------------------*/
unsigned char data TxBuf[12]; //设置发送长度，最高为32字节	 
unsigned char data RxBuf[12]; //设置接收长度，最高为32字节
//*****************飞控控制参数**************************
unsigned int YM=0;//油门变化速度控制，不这样做的话快速变化油门时四轴会失速翻转并GG
int speed0=0,speed1=0,speed2=0,speed3=0;   //电机速度参数
int PWM0=0,PWM1=0,PWM2=0,PWM3=0;//加载至PWM模块的参数
double g_x=0,g_y=0,g_z=0;       //陀螺仪矫正参数
char a_x=0,a_y=0,a_z=0;         //角度矫正参数
float FR1=0,FR2=0,FR3=0;	    //方向控制数据变量

//*****************MPU-6050 寄存器数据参数**************************
double Gyro_y=0,Gyro_x=0,Gyro_z=0;        //Y轴陀螺仪数据暂存
double Accel_x=0,Accel_y=0,Accel_z=0;	  //X轴加速度值暂存
double Angle_ax=0,Angle_ay=0,Angle_az=0;  //由加速度计算的加速度(弧度制)
double Angle_gy=0,Angle_gx=0,Angle_gz=0;  //由角速度计算的角速率(角度制)

//==================================================//
//   PID算法变量
//==================================================//
int    data AngleX=0,AngleY=0;		//四元数解算出的欧拉角
float  data PID_Output;				//PID最终输出量

float xdata Ax=0,Ay=0;Az=0;			//加入遥控器控制量后的角度
float xdata gx=0,gy=0;				//加入遥控器控制量后的角度
float xdata ERRORX_In=0;			//内环P 内环I 内环D 内环误差积分
float xdata ERRORX_Out=0;			//外环P 外环I       外环误差积分

float xdata ERRORY_In=0;
float xdata ERRORY_Out=0;

float xdata ERRORZ_Out=0;
float xdata Anglezlate=0;			//Z轴相关

float xdata Last_Ax=0;
float xdata Last_Ay=0;

float xdata Last_gx=0;
float xdata Last_gy=0;


//==================================================//
//   PID 手动微调参数值
//==================================================//
// D值只要不超过10都可以，D值在3以上10以下!!! D值不合适飞机就会荡
#define	Out_XP	15.0f	//外环P
#define	Out_XI	0.01f	//外环I
#define	Out_XD	5.0f	//外环D

#define	In_XP	0.55f	//内环P 720
#define	In_XI	0.01f	//内环I
#define	In_XD	3.0f	//内环D 720

#define	In_YP	In_XP
#define	In_YI	In_XI
#define	In_YD	In_XD

#define	Out_YP	Out_XP
#define	Out_YI	Out_XI
#define	Out_YD	Out_XD

//float ZP=5.0,ZD=4.0;	//自旋控制的P D
#define	ZP	3.0f
#define	ZD	1.0f	//自旋控制的P D

#define	ERR_MAX	800	 				//

//--------------------------------------------------//
//  PID算法飞控自平衡 函数
//--------------------------------------------------//

void Flight(void)interrupt 1 
{
	
//  读取MCU6050 寄存器数据
	Gyro_x = GetData(GYRO_XOUT_H)-g_x;//读出 X轴陀螺仪数据
	Gyro_y = GetData(GYRO_YOUT_H)-g_y;//读出 Y轴陀螺仪数据
	Gyro_z = GetData(GYRO_ZOUT_H)-g_z;//读出 Z轴陀螺仪数据	
	Accel_y= GetData(ACCEL_YOUT_H);//读出 X轴加速度数据
	Accel_x= GetData(ACCEL_XOUT_H);//读出 Y轴陀螺仪数据		   
	Accel_z= GetData(ACCEL_ZOUT_H);//读出 Z轴加速度数据	     
	//姿态数据算法 （借鉴STC官方算法）
//	Last_Angle_gx=Angle_gx;   //储存上一次角速度数据
//	Last_Angle_gy=Angle_gy;	  //储存上一次角速度数据
	Angle_ax=(Accel_x)/8192;  //加速度处理
	Angle_az=(Accel_z)/8192;  //加速度量程 +-4g/S
	Angle_ay=(Accel_y)/8192;  //转换关系   8192LSB/g
	Angle_gx=(Gyro_x)/65.5;   //陀螺仪处理
	Angle_gy=(Gyro_y)/65.5;   //陀螺仪量程 +-500度/S
	Angle_gz=(Gyro_z)/65.5;   //转换关系65.5LSB/度
//***********************************四元数解算***********************************
	IMUupdate(Angle_gx*0.0174533,Angle_gy*0.0174533,Angle_gz*0.0174533,Angle_ax,Angle_ay,Angle_az);//0.174533为PI/180 目的是将角度转弧度
//****飞控失联判断 自动降落算法***********************************************************************
//接收遥控器发来的不断更新数据 判断联机通讯是否正常
	if(SSLL==ShiLian)	//如果SSLL的数据没有更新即失联
	{ if(++SLJS>=20)
		{	SLJS = 19;	    //状态标识
			HangXiang=128;	//航向变量 
			HengGun  =128;	//横滚变量 
			FuYang   =128;	//俯仰变量
			if(YM>20)YM--;//油门在原值逐渐减小	
		}
	}
	else {SLJS=0;if(YouMen>1001)YouMen=1000;else YM=YouMen;}//输入油门量0-1000
	ShiLian = SSLL;
	//油门优化算法-后期更新
//****以下是飞行控制算法***********************************************************************
//************** MPU6050 X轴指向 ***********************************************************
//	FR1=0;//关闭横滚
	FR1=((float)HengGun-128)/4; //得到 横滚数据变量
	Ax=-FR1+a_x-AngleX;   //角度控制量加载至角度


	if(YM > 30)	ERRORX_Out += Ax;	//外环积分(油门小于某个值时不积分)
	else		ERRORX_Out = 0;		//油门小于定值时清除积分值

		 if(ERRORX_Out >  ERR_MAX)	ERRORX_Out =  ERR_MAX;	//积分限幅
	else if(ERRORX_Out < -ERR_MAX)	ERRORX_Out = -ERR_MAX;	//积分限幅
	
	PID_Output = Ax*Out_XP + ERRORX_Out*Out_XI+(Ax-Last_Ax)*Out_XD;	//外环PID
	Last_Ax=Ax;
//	if(YM > 20)	ERRORX_In += (Angle_gy - PID_Output);	//内环积分(油门小于某个值时不积分)
	gy=PID_Output - Angle_gy;


	if(YM > 30)	ERRORX_In += gy;	//内环积分(油门小于某个值时不积分)
	else		ERRORX_In  = 0; //油门小于定值时清除积分值

		 if(ERRORX_In >  ERR_MAX)	ERRORX_In =  ERR_MAX;
	else if(ERRORX_In < -ERR_MAX)	ERRORX_In = -ERR_MAX; //积分限幅
	
//	PID_Output = (Angle_gy + PID_Output)*In_XP + ERRORX_In*In_XI + (Angle_gy - Last_Angle_gy)*In_XD;	//内环PID
	PID_Output = gy*In_XP + ERRORX_In*In_XI + (gy-Last_gy)*In_XD;
	Last_gy=gy;
	if(PID_Output >  1000)	PID_Output =  1000;  //输出量限幅
	if(PID_Output < -1000)	PID_Output = -1000;
	
	speed0 = 0 + PID_Output;	speed1 = 0 - PID_Output;
	speed3 = 0 + PID_Output;	speed2 = 0 - PID_Output;
//**************MPU6050 Y轴指向**************************************************
	FR2=((float)FuYang-128)/4; //得到 俯仰数据变量
	Ay=-FR2-a_y-AngleY;      //角度控制量加载至角度

	if(YM > 30)		ERRORY_Out += Ay;				//外环积分(油门小于某个值时不积分)
	else			ERRORY_Out = 0;					//油门小于定值时清除积分值
	if(ERRORY_Out >  ERR_MAX)	ERRORY_Out =  ERR_MAX;
	else if(ERRORY_Out < -ERR_MAX)	ERRORY_Out = -ERR_MAX;			//积分限幅
	
	PID_Output = Ay*Out_YP + ERRORY_Out*Out_YI+(Ay-Last_Ay)*Out_YD;	//外环PID
	Last_Ay=Ay;
	gx=PID_Output - Angle_gx;
	if(YM > 30)ERRORY_In +=gx;								//内环积分(油门小于某个值时不积分)
	else			ERRORY_In = 0;							//油门小于定值时清除积分值
		 if(ERRORY_In >  ERR_MAX)	ERRORY_In =  ERR_MAX;
	else if(ERRORY_In < -ERR_MAX)	ERRORY_In = -ERR_MAX;	//积分限幅
	
//	PID_Output = (Angle_gx + PID_Output)*In_YP + ERRORY_In*In_YI + (Angle_gx - Last_Angle_gx)*In_YD;	//内环PID
	PID_Output = gx*In_YP + ERRORY_In*In_YI + (gx - Last_gx)*In_YD;
	Last_gx=gx;
	if(PID_Output >  1000)	PID_Output =  1000;  //输出量限幅
	if(PID_Output < -1000)	PID_Output = -1000;
	
	speed0 = speed0 + PID_Output;	speed1 = speed1 + PID_Output;//加载到速度参数
	speed3 = speed3 - PID_Output;	speed2 = speed2 - PID_Output;

//************** MPU6050 Z轴指向 *****************************	

//	Angle_gz = ((float)RxBuf[3] - 128) * 1.0f-Angle_gz;	//操作量

	FR3=((float)HangXiang-128)*1.5;//得到 航向数据变量
	Az=FR3+a_z-Angle_gz;
	if(YM > 30)		ERRORZ_Out += Az;
	else			ERRORZ_Out  = 0; 
	if(ERRORZ_Out >  800)	ERRORZ_Out =  800;
	else if(ERRORZ_Out < -800)	ERRORZ_Out = -800;	//积分限幅
	PID_Output = Az*ZP + ERRORZ_Out * 0.2f + (Az - Anglezlate) * ZD;

	Anglezlate = Az;
	speed0 = speed0 + PID_Output;	speed1 = speed1 - PID_Output;
	speed3 = speed3 - PID_Output;	speed2 = speed2 + PID_Output;

//**************将速度参数加载至PWM模块*************************************************	
	//速度参数控制，防止超过PWM参数范围0-1000（X型有效）
	PWM0=(YM+speed0);if(PWM0>1000)PWM0=1000;else if(PWM0<0)PWM0=0;
	PWM1=(YM+speed1);if(PWM1>1000)PWM1=1000;else if(PWM1<0)PWM1=0;
	PWM2=(YM+speed2);if(PWM2>1000)PWM2=1000;else if(PWM2<0)PWM2=0;
	PWM3=(YM+speed3);if(PWM3>1000)PWM3=1000;else if(PWM3<0)PWM3=0;
 	
	//满足条件：（解锁：2.4G=5；油门大于50）才能控制电机
	if(JieSuo==5&&YM>=30)
	  {PWM(1000-PWM0,1000-PWM1,1000-PWM2,1000-PWM3);} //启动PWM
	else	  
	  {PWM(1000,1000,1000,1000);}                     //关闭PWM
} 
//--------------------------------------------------//
//  时间延时 函数
//--------------------------------------------------//
void Delay(unsigned int x)
{unsigned int i,j;
	for(i=0;i<x;i++)
	for(j=0;j<250;j++);
}
//--------------------------------------------------//
//  飞控所有设置存储数据变量复位 函数
//--------------------------------------------------//
void init_FKCanShu(void) 
{
	unsigned char data DD[10];
	a_x=0;//手动微调 X轴 复位初始值
	a_y=0;//手动微调 Y轴 复位初始值
	a_z=0;//手动微调 Z轴 复位初始值
	DD[0]=128;//存储 X轴 复位初始值
	DD[1]=128;//存储 Y轴 复位初始值
	DD[2]=128;//存储 Z轴 复位初始值
	EEPROM_Xie(DD);//存储 手动校准参数值到EEPROM
}
//--------------------------------------------------//
//  定时器0 初始化函数 V2.0
//--------------------------------------------------//
void Timer0Init(void)	//10微秒@27.000MHz
{
	TR0 = 0;
	AUXR &= 0x7F;	//定时器时钟12T模式
	TMOD &= 0xF0;	//设置定时器模式
	IE  = 0x82;
	TL0 = 0x1C;		//设置定时初值
	TH0 = 0xA8;		//设置定时初值
	TF0 = 0;		//清除TF0标志
	TR0 = 1;		//定时器0开始计时

}
	
//--------------------------------------------------//
//  程序 主函数
//--------------------------------------------------//
void main(void)
{
	unsigned char GGGXYZ[5];//手动校准修改变量
	unsigned char DCDY;//电池电压 变量

	/*----华丽的分割线----华丽的分割线----华丽的分割线----华丽的分割线----*/
	PWMGO();		//初始化PWM
	LEDH1=0;LEDH2=1;LEDH3=1;LEDH4=1;Delay(1000);//控制航向灯
	LEDH1=1;LEDH2=0;LEDH3=1;LEDH4=1;Delay(1000);//控制航向灯
	LEDH1=1;LEDH2=1;LEDH3=0;LEDH4=1;Delay(1000);//控制航向灯
	LEDH1=1;LEDH2=1;LEDH3=1;LEDH4=0;Delay(1000);//控制航向灯
	LEDH1=0;LEDH2=1;LEDH3=1;LEDH4=1;Delay(1000);//控制航向灯
	LEDH1=1;LEDH2=0;LEDH3=1;LEDH4=1;Delay(1000);//控制航向灯
	LEDH1=1;LEDH2=1;LEDH3=0;LEDH4=1;Delay(1000);//控制航向灯
	LEDH1=1;LEDH2=1;LEDH3=1;LEDH4=0;Delay(1000);//控制航向灯
	LEDH1=1;LEDH2=1;LEDH3=1;LEDH4=1;Delay(1000);//控制航向灯
	LEDZ1=0;LEDZ2=1;Delay(1000);//控制 状态灯
	LEDZ1=1;LEDZ2=0;Delay(1000);//控制 状态灯
	LEDZ1=1;LEDZ2=1;Delay(1000);//控制 状态灯
	Delay(10);    // 延时 100
//	InitADC();		//ADC模数转换 初始化（后期开发）
	Delay(10);    // 延时 100
	Init_MPU6050();	//初始化MPU-6050
	Delay(10);    // 延时 100
	if(KASB==0)init_FKCanShu();//开机按下飞控按键 进入微调参数变量初始值
	Delay(10);    // 延时 100
	init_NRF24L01() ;  //NRF24L01 初始化
	nRF24L01_RX_Mode(RxBuf);//数据接收
	Delay(100);	  //延时一会 1S
//---------------------------------------//
	Timer0Init(); //初始化定时器
	Delay(100);	  //延时一会 1S
	/*----华丽的分割线----华丽的分割线----华丽的分割线----华丽的分割线----*/
	EEPROM_Du(GGGXYZ);//读出EEPROM到 手动校准参数值
	a_x=GGGXYZ[0]-128;//读出 X轴保存值
	a_y=GGGXYZ[1]-128;//读出 Y轴保存值
	a_z=GGGXYZ[2]-128;//读出 Z轴保存值
	Delay(10);    // 延时 100
	YouMen =0;		//初始化油门变量 
	HangXiang=128;	//初始化航向变量 
	HengGun =128;	//初始化横滚变量 
	FuYang  =128;	//初始化俯仰变量
	FR1=0;FR2=0;FR3=0;//初始化飞行控制变量
 	/*----华丽的分割线----华丽的分割线----华丽的分割线----华丽的分割线----*/
	LEDZ1=1; //关闭状态灯
	EA = 1;  //开总中断
while(1)
{
/*----华丽的分割线----华丽的分割线----华丽的分割线----华丽的分割线----*/
//	TxBuf[0]=SSLL;		//发送 失联变量
//	TxBuf[1]=JieSuo;	//发送 返回命令值
	TxBuf[2]=((int)AngleX)+128;	//发送 横滚角度
	TxBuf[3]=((int)AngleY)+128;	//发送 俯仰角度
	TxBuf[4]=DCDY;		//发送 电池电压
//	TxBuf[5]=;		//发送
//	TxBuf[6]=;		//发送
//-------------------------------------------------//
    //判断遥控发来的存储命令 =1
	if(RxBuf[7]==1){RxBuf[7]=0;//复位结果值
	TxBuf[7]=2;		  //发送 变量
	EEPROM_Du(GGGXYZ);//读出EEPROM到 手动校准参数值
	a_x=GGGXYZ[0]-128;//读出 X轴保存值
	a_y=GGGXYZ[1]-128;//读出 Y轴保存值
	a_z=GGGXYZ[2]-128;//读出 Z轴保存值
	TxBuf[8]=a_x+128;//读出 X轴保存值
	TxBuf[9]=a_y+128;//读出 Y轴保存值
	TxBuf[10]=a_z+128;//读出 Z轴保存值
	}	
//-------------------------------------------------//
	nRF24L01_TX_Mode(TxBuf);//加载目标地址 数据发送
	Delay(50);
	nRF24L01_RX_Mode(RxBuf);//加载自己地址 数据接收
	Delay(50);

	SSLL     =RxBuf[0];	 //接收 失联变量
	JieSuo   =RxBuf[1];	 //接收 命令值
	YouMen	 =RxBuf[2]*0xff+RxBuf[3];  //接收 油门变量
	HangXiang=RxBuf[4];	 //接收 航向变量 
	HengGun  =RxBuf[5];	 //接收 横滚变量 
	FuYang   =RxBuf[6];	 //接收 俯仰变量
//-------------------------------------------------//
    //判断遥控发来的存储命令 =1 保存手动微调参数
	if(RxBuf[7]==3){RxBuf[7]=0;  //复位结果值
	TxBuf[7]=4;					 //发送 变量
	a_x=RxBuf[8]-128;  //接收 横滚手动值 
	a_y=RxBuf[9]-128;  //接收 俯仰手动值
	a_z=RxBuf[10]-128;  //接收 航向手动值
	GGGXYZ[0]=a_x+128; //接收 横滚手动值 
	GGGXYZ[1]=a_y+128; //接收 俯仰手动值
	GGGXYZ[2]=a_z+128; //接收 航向手动值
	EEPROM_Xie(GGGXYZ);//存储 手动校准参数值到EEPROM
	}
//-------------------------------------------------//
    //判断遥控发来的存储命令 =A 自动校准
	if(RxBuf[7]==0x0A){RxBuf[7]=0;  //复位结果值
	TxBuf[7]=0xA1;					//发送 变量
	g_x=Gyro_x;	//读出当前值
	g_y=Gyro_y;	//读出当前值
	g_x=Gyro_x;	//读出当前值
	}//此校准方法-不太合理（等待后期更新v3.0）

/*----华丽的分割线----华丽的分割线----华丽的分割线----华丽的分割线----*/
	if(JieSuo==3){LEDZ2=0;}else	{LEDZ2=1;}//点亮状态灯
	if(JieSuo==5){LEDH1=0;LEDH2=0;LEDH3=0;LEDH4=0;}//点亮航向灯
	else	     {LEDH1=1;LEDH2=1;LEDH3=1;LEDH4=1;}//关闭航向灯
/*----华丽的分割线----华丽的分割线----华丽的分割线----华丽的分割线----*/
}}
