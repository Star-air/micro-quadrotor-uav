#ifndef __OLED9664_H__
#define __OLED9664_H__

/*********************************************************/
void OLED_SSD1305Init(void);  //OLED初始化   
void OLED_CLS(void);  //OLED复位清屏
void OLED_shuzi(unsigned char x, unsigned char y,unsigned char T);
//void OLED_P8x16(unsigned char x, unsigned char y,unsigned char *T);
//void OLED_P16x16(unsigned char x, unsigned char y,unsigned char *T);
void ZiKuIC_init(void);	 //字库IC初始化
void GB2312_OLED(unsigned char x,unsigned char y,unsigned char *text);
#endif