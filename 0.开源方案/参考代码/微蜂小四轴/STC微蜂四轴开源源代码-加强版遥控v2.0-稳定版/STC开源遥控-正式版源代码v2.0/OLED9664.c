/*********************************************************
OLED屏测试程序 
*********************************************************/
#include <STC15F2K60S2.h>	//STC15W4K48S4 专用头文件
#include "OLED9664.h"	    //OLED96x64 白色点阵屏
#include "OLED9664KU.h"
#include "ZiKuIC.h"			//字库IC 专用驱动程序
extern void          _nop_     (void);
/*-----------------------------------------------
STC单片机 硬件设备引脚定义如下
-----------------------------------------------*/
sbit OLED_DC =P2^4; //数据/命令控制
sbit OLED_RST=P2^5; //复位 
sbit OLED_SDA=P2^6; //D1（MOSI） 数据
sbit OLED_SCL=P2^7; //时钟 D0（SCLK）
/*********************OLED写数据************************************/ 
void OLED_WrDat(unsigned char dat)	 
{
unsigned char i;
OLED_DC=1;  
for(i=0;i<8;i++) //发送一个八位数据 
{
if((dat << i) & 0x80)
{OLED_SDA  = 1;}
else  OLED_SDA  = 0;
OLED_SCL = 0;
OLED_SCL = 1;
}
}
/*********************OLED写命令************************************/										
void OLED_WrCmd(unsigned char cmd)
{
unsigned char i;
OLED_DC=0;
for(i=0;i<8;i++) //发送一个八位数据 
{
if((cmd << i) & 0x80)
{OLED_SDA  = 1;}
else  OLED_SDA  = 0;
OLED_SCL = 0;
OLED_SCL = 1;
}
}
/*********************OLED 设置坐标************************************/
void OLED_SetPos(unsigned char x, unsigned char y) 
{ 

OLED_WrCmd(0xB0+y);		    //写入 64行【0xB0~0xF0】
if(x&0x01)OLED_WrCmd(0x0A);	//写入低8列【0x0A~0x12】
else      OLED_WrCmd(0x02);	//写入低8列【0x02~0x09】 
OLED_WrCmd(0x11+x/2);	   	//写入高8列【0x11~0x16】
} 
/*******************功能描述：显示8*16一组标准ASCII字符串	 显示的坐标（x,y），y为页范围0～7****************/
void OLED_shuzi(unsigned char x, unsigned char y,unsigned char T)
{
unsigned char i=0;		   //变量循环
OLED_SetPos(x,y*2);    	   //写入坐标
for(i=0;i<8;i++)     	   //循环8次
OLED_WrDat(shu[T][i]);	   //写入数据 0-8行
OLED_SetPos(x,y*2+1);      //写入坐标
for(i=0;i<8;i++)     	   //循环8次
OLED_WrDat(shu[T][i+8]);   //写入数据 9-16行
}
/*******************功能描述：显示8*16一组标准ASCII字符串	 显示的坐标（x,y），y为页范围0～7****************/
void OLED_P8x16(unsigned char x, unsigned char y,unsigned char *T)
{
unsigned char i=0;		   //变量循环
OLED_SetPos(x,y*2);    	   //写入坐标
for(i=0;i<8;i++)     	   //循环8次
OLED_WrDat(T[i]);	   //写入数据 0-8行
OLED_SetPos(x,y*2+1);      //写入坐标
for(i=0;i<8;i++)     	   //循环8次
OLED_WrDat(T[i+8]);   //写入数据 9-16行
}

/*功能描述：显示16*16一组标准汉字	 显示的坐标（x,y），y为页范围0～7****************/
void OLED_P16x16(unsigned char x, unsigned char y,unsigned char *T)
{
unsigned char i=0;
OLED_WrCmd(0xB0+y*2);	//写入 64行【0xB0~0xF0】
if(x&0x01)OLED_WrCmd(0x0A);	//写入低8列【0x0A~0x12】
else      OLED_WrCmd(0x02);	//写入低8列【0x02~0x09】 
OLED_WrCmd(0x11+x/2);	   	//写入高8列【0x11~0x16】
for(i=0;i<16;i++)     	//变量循环
OLED_WrDat(T[i]);	//写入数据 0-8行
OLED_WrCmd(0xB0+y*2+1);	//写入 64行【0xB0~0xF0】
if(x&0x01)OLED_WrCmd(0x0A);	//写入低8列【0x0A~0x12】
else      OLED_WrCmd(0x02);	//写入低8列【0x02~0x09】 
OLED_WrCmd(0x11+x/2);	   	//写入高8列【0x11~0x16】
for(i=0;i<16;i++)		//变量循环
OLED_WrDat(T[i+16]);//写入数据 9-16行 
}
/*********************OLED复位清屏************************************/
void OLED_CLS(void)
{
unsigned char i,j;
  for(j=0;j<8;j++)
  {
       OLED_WrCmd(0xB0+j);    	//set page address
       OLED_WrCmd(0x00);    	//set lower column address
       OLED_WrCmd(0x10);    	//set higher column address
       for(i=0;i<132;i++)
       {
        OLED_WrDat(0x00);
       }
  }
}
/*********************OLED初始化************************************/
void  OLED_SSD1305Init(void)
{
	OLED_SCL=1;
	OLED_RST=0;
	_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();
	_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();
	_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();
	_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();
	_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();
	OLED_RST=1;       //从上电到下面开始初始化要有足够的时间，即等待RC复位完毕   
	OLED_WrCmd(0xae);    /*display off*/
	OLED_WrCmd(0x02);    /*set lower column address*/
	OLED_WrCmd(0x11);    /*set higher column address*/
	OLED_WrCmd(0x40);    /*set display start line*/
	OLED_WrCmd(0xB0);    /*set page address*/
	OLED_WrCmd(0x81);    /*contract control*/
	OLED_WrCmd(0xcf);    /*128*/
	OLED_WrCmd(0xA1);    /*set segment remap*/
	OLED_WrCmd(0xA6);    /*normal / reverse*/
	OLED_WrCmd(0xA8);    /*multiplex ratio*/
	OLED_WrCmd(0x3F);    /*duty = 1/64*/	
	OLED_WrCmd(0xC8);    /*Com scan direction*/
	OLED_WrCmd(0xD3);    /*set display offset*/
	OLED_WrCmd(0x00);
	OLED_WrCmd(0xD5);    /*set osc division*/
	OLED_WrCmd(0x80);	   //80	71
	OLED_WrCmd(0xD9);    /*set pre-charge period*/
	OLED_WrCmd(0xf1);
	OLED_WrCmd(0xDA);    /*set COM pins*/
	OLED_WrCmd(0x12);
	OLED_WrCmd(0xdb);    /*set vcomh*/
	OLED_WrCmd(0x40);
	OLED_WrCmd(0x8d);    /*set charge pump enable*/
	OLED_WrCmd(0x14);
	OLED_WrCmd(0xAF);    /*display ON*/
}

/*------------------------------------------------
          字库读取显示函数
 字库地址算法fontaddr += (text[i+1]-0xa1)+X；
 X=写入地址*8；	=》0x0110X8=2176；
------------------------------------------------*/
void GB2312_OLED(unsigned char x,unsigned char y,unsigned char *text)
{
unsigned char i= 0;
unsigned char fontbuf[32];
unsigned long fontaddr=0;

  while((text[i]>0x00))
	{
	
	if(((text[i]>=0xb0) &&(text[i]<=0xf7))&&(text[i+1]>=0xa1))
	{
	//国标简体（GB2312）汉字在晶联讯字库IC  中的地址由以下公式来计算：
	fontaddr = (text[i]- 0xb0)*94;	//每一组的汉字数量
	fontaddr += (text[i+1]-0xa1)+3328; //0x01a0xx
	fontaddr = (unsigned long)(fontaddr*32);
	ZiKuIC_Read_Address(fontaddr,fontbuf,32 );//取32 个字节的数据
	//存到"fontbuf[32]"
	OLED_P16x16(x,y,fontbuf);
	//显示汉字到LCD 上，y 为页地址，x 为列地址，fontbuf[] 为数据
	i+=2;
	x+=2;
	}
else if(((text[i]>=0x81)&&(text[i]<=0xA0))&&((text[i+1]>=0x40)&&(text[i+1]<=0x7e)))
	{
	//国标简体（GB2312）汉字在晶联讯字库IC  中的地址由以下公式来计算：
	fontaddr = (text[i]-0x81)*63;	 //每一组的汉字数量
	fontaddr += (text[i+1]-0x40)+10112; //0x04f0xx
	fontaddr = (unsigned long)(fontaddr*32);
	ZiKuIC_Read_Address(fontaddr,fontbuf,32 );//取32 个字节的数据
	//存到"fontbuf[32]"
	OLED_P16x16(x,y,fontbuf);
	//显示汉字到LCD 上，y 为页地址，x 为列地址，fontbuf[] 为数据
	i+=2;
	x+=2;
	}
else if(((text[i]>=0x81)&&(text[i]<=0xA0))&&((text[i+1]>=0x80)&&(text[i+1]<=0xfe)))
	{
	//国标简体（GB2312）汉字在晶联讯字库IC  中的地址由以下公式来计算：
	fontaddr = (text[i]-0x81)*127;	 //每一组的汉字数量
	fontaddr += (text[i+1]-0x80)+12128; //0x05ecxx
	fontaddr = (unsigned long)(fontaddr*32);
	ZiKuIC_Read_Address(fontaddr,fontbuf,32 );//取32 个字节的数据
	//存到"fontbuf[32]"
	OLED_P16x16(x,y,fontbuf);
	//显示汉字到LCD 上，y 为页地址，x 为列地址，fontbuf[] 为数据
	i+=2;
	x+=2;
	}
else if(((text[i]>=0xAA)&&(text[i]<=0xFE))&&((text[i+1]>=0x40)&&(text[i+1]<=0x7e)))
	{
	//国标简体（GB2312）汉字在晶联讯字库IC  中的地址由以下公式来计算：
	fontaddr = (text[i]-0xAA)*63;	 //每一组的汉字数量
	fontaddr += (text[i+1]-0x40)+16208; //0x07EAxx
	fontaddr = (unsigned long)(fontaddr*32);
	ZiKuIC_Read_Address(fontaddr,fontbuf,32 );//取32 个字节的数据
	//存到"fontbuf[32]"
	OLED_P16x16(x,y,fontbuf);
	//显示汉字到LCD 上，y 为页地址，x 为列地址，fontbuf[] 为数据
	i+=2;
	x+=2;
	}
else if(((text[i]>=0xAA)&&(text[i]<=0xFE))&&((text[i+1]>=0x80)&&(text[i+1]<=0xA0)))
	{
	//国标简体（GB2312）汉字在晶联讯字库IC  中的地址由以下公式来计算：
	fontaddr = (text[i]-0xAA)*33;	 //每一组的汉字数量
	fontaddr += (text[i+1]-0x80)+21584; //0x0A8Axx
	fontaddr = (unsigned long)(fontaddr*32);
	ZiKuIC_Read_Address(fontaddr,fontbuf,32 );//取32 个字节的数据
	//存到"fontbuf[32]"
	OLED_P16x16(x,y,fontbuf);
	//显示汉字到LCD 上，y 为页地址，x 为列地址，fontbuf[] 为数据
	i+=2;
	x+=2;
	}	
else if(((text[i]>=0xa1)&&(text[i]<=0xa9))&&(text[i+1]>=0xa1))
	{
	/*国标简体（GB2312）15x16 点的字符在晶联讯字库
	IC 中的地址由 以下公式来计算：*/
	/*Address = ((MSB - 0xa1) * 94 + 
	(LSB - 0xA1))*32+ BaseAdd;BaseAdd=0*/
	/*由于担心8 位单片机有乘法溢出问题，所以分三部取地址*/
	fontaddr = (text[i]- 0xa1)*94;
	fontaddr += (text[i+1]-0xa1)+2176;//地址0x0110；
	fontaddr = (unsigned long)(fontaddr*32);
	ZiKuIC_Read_Address(fontaddr,fontbuf,32 );//取32 个字节的数据
	//存到"fontbuf[32]"
	OLED_P16x16(x,y,fontbuf);
	//显示汉字到LCD 上，y 为页地址，x 为列地址，fontbuf[] 为数据
	i+=2;
	x+=2;
  }
else if((text[i]>=0x20) &&(text[i]<=0x7e))
	{ /*ASCII 码字符的8*16 点阵在字库IC 中的地址*/
	fontaddr = (text[i]- 0x20)*16;
	fontaddr = fontaddr+0x010000;//0x0100xx
	ZiKuIC_Read_Address(fontaddr,fontbuf,16 );
	/*取16 个字节的数据，存到"fontbuf[32]"*/
	OLED_P8x16(x,y,fontbuf);/*显示8x16 的ASCII 字到LCD 上，y 为
	//页地址，x 为列地址，fontbuf[]为数据*/
	i++;
	x+=1;
	}
	else i++;
	}
}		

