#ifndef __ZiKuIC_H__
#define __ZiKuIC_H__
/***********************************************************************
程序功能: 21116d 字库IC 专用驱动程序
**********************************************************************/
extern void          _nop_     (void);
/***********************************************************************
设置按键 IO口定义
***********************************************************************/
sbit    ZiKuIC_CLK   = P1^5;	 //(器件6脚)SPI时钟输入
sbit    ZiKuIC_DI0   = P1^6;	 //(器件5脚)SPI数据输入/输出
sbit    ZiKuIC_DO    = P1^7;	 //(器件2脚)SPI数据输出
sbit    ZiKuIC_CS    = P5^4;	 //(器件1脚)SPI片选
/************************************************************************************************/
void ZiKuIC_Send_Byte(unsigned char out)			 //ZiKuIC 写函数
{    unsigned char i = 0;    
    for (i = 0; i < 8; i++)
    {   
	    if ((out & 0x80) == 0x80)          
            ZiKuIC_DI0 = 1;
        else
            ZiKuIC_DI0 = 0;                   
        ZiKuIC_CLK = 1;                    
        out = (out << 1);                  
        _nop_();_nop_();_nop_();
		ZiKuIC_CLK = 0;
    }

}
/************************************************************************************************/
unsigned char ZiKuIC_Get_Byte(void)		//读函数
{    unsigned char i = 0, in = 0, temp = 0;    
    for (i = 0; i < 8; i++)
    {    in = (in << 1);		//shift 1 place to the left or shift in 0
        temp = ZiKuIC_DO;		//save input
        ZiKuIC_CLK = 1;			//toggle clock high
        if (temp == 1)			//check to see if bit is high
            in |= 0x01;			//if high, make bit high
        ZiKuIC_CLK = 0;			//toggle clock low
    }    
    return in;
}
//************************************************************************************************/

void ZiKuIC_Write_Disable(void)   //写禁能
{    ZiKuIC_CS = 0;               //片选 拉低
    ZiKuIC_Send_Byte(0x04);       //写入0x04H 
    ZiKuIC_CS = 1;                //片选 拉高
}
/************************************************************************************************/
void ZiKuIC_init(void)	 	//字库IC 初始化
{    
    ZiKuIC_CLK = 0;    	 	//时钟 拉低
    ZiKuIC_Write_Disable(); //写禁能   
}
/************************************************************************************************/
void ZiKuIC_Read_Address(unsigned long  Dst_Addr,unsigned char *pBuff,unsigned char k)//读 字节数据
{   
    unsigned char i = 0;    
    ZiKuIC_CS = 0;          										//片选 拉低
    ZiKuIC_Send_Byte(0x03); 										//写地址 0x03H
    ZiKuIC_Send_Byte((unsigned char)((Dst_Addr & 0xFFFFFF) >> 16)); //写 24位地址
    ZiKuIC_Send_Byte((unsigned char)((Dst_Addr & 0xFFFF) >> 8));	//写 24位地址
    ZiKuIC_Send_Byte((unsigned char)(Dst_Addr & 0xFF));			    //写 24位地址
	for(i = 0; i <k; i++ )pBuff[i] =ZiKuIC_Get_Byte();				//读出 一个字节数据
    ZiKuIC_CS = 1;                 									//片选 拉高    
}
#endif