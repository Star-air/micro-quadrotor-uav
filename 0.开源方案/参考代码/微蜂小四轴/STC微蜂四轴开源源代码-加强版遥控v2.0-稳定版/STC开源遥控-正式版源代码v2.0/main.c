/*------------------------------------------------------------
此产品是基于STC宏晶公司，在2015年最新开发出一款STC15W4K（IAP15W4K）系列的
8位单片机。此系列在STC12系列的基础上 增加了宽电压和多路硬件PWM功能（15位）。
此条件满足了四轴飞行的基本设计需求，经过本人多方面学习与研究的过程中，
掌握了迷你四轴的飞行原理和制作具体要求设计参数等。

此开源源代程序码-是“微蜂四轴”专用程序，基于STC15W4Kxx设计的STC迷你四轴飞行器。
在本程序中采用全中文段落注解方法，方便入门或者单片机玩家，学习与交流。
想全面了解四轴飞行器的飞行原理和程序代码，可上淘宝网采购此产品做深入学习。
本程序是不断更新的程序源代码，想拿到最新，飞行最稳定版，请关注我们相关宣传。

以下是“微蜂四轴”的程序更能说明：
单片机采用STC15W4K48S4-增强型单片机（IPA需修改EEPROM代码）
工作电压为：5V	晶振频率为：27M	无线通讯模块为：NRF24L01+）
本程序中 采用了MCU6050做姿态数据采集加PID算法给硬件4路PWM控制MOS管驱动空心杯电机
注意：此程序只兼容：空心杯716电机与45MM专用正反桨（自己的电机或需修改PID参数）
在后期的程序更新中，会增加硬件代码达到预期飞行功能。敬请关注。

开源程序-广大四轴爱好者都可以进行修改与技术交流。
您如果在此基础上能调出更好的飞行效果，可与设计者联系，有丰厚实物奖励哦。
原创设计者：联系方式QQ：250278069  
STC迷你四轴技术交流群：421109612
淘宝网站：http://shop117773209.taobao.com/shop/view_shop.htm?spm=a1z0e.1.0.0.eqIhKz&mytmenu=mdianpu&utkn=g,meztsobuhe3to1427461542456&user_number_id=669230514&scm=1028.1.1.20001
制作完成时间：2015年11月20日-V2.5版
------------------------------------------------------------*/
#include <STC15F2K60S2.h>	//STC15W4K48S4 专用头文件
#include "STC15W4K-ADC.h"	//STC15W4K-ADC	硬件ADC模数转换
#include "NRF24L01.h"	    //NRF24L01 2.4G无线收发模块
#include "OLED9664.h"	    //OLED96x64 白色点阵屏
/***********************************************************************
设置按键 IO口定义
**********************************************************************/
sbit	KAL   =P3^6; //左边按键按键 L
sbit	KAR   =P3^3; //右边按键按键 R

sbit	KS1   =P0^0; //功能键 A 
sbit	KS2   =P0^1; //功能键 B 
sbit	KS3   =P0^2; //功能键 C 
sbit	KS4   =P0^3; //功能键 D

sbit	KS5   =P0^4; //导航 上键 
sbit	KS6   =P0^7; //导航 下键 
sbit	KS7   =P0^5; //导航 左键 
sbit	KS8   =P0^6; //导航 右键 

sbit	Feng  =P5^5; //蜂鸣器 

sbit	LEDA  =P3^7; //独立LED 蓝
sbit	LEDR  =P2^2; //全彩LED R红 
sbit	LEDG  =P2^3; //全彩LED G绿
sbit	LEDB  =P2^1; //全彩LED B蓝
/*------------------------------------------------
                  全局函数定义
------------------------------------------------*/
unsigned char TxBuf[12]; //设置发送长度，最高为32字节	 
unsigned char RxBuf[12]; //设置接收长度，最高为32字节
/*------------------------------------------------
                  延时函数
------------------------------------------------*/
void Delay(unsigned char s)
{
	unsigned char i,j;
	for(i=0; i<s; i++)
		for(j=0; j<250; j++);
}
/*------------------------------------------------
  OLED开机函数 
------------------------------------------------*/
void OLEDKaiJi(void)
{
	//开机问候语
	GB2312_OLED(0,0,"学习开源飞控");
	GB2312_OLED(0,1,"迷你微蜂四轴");
	GB2312_OLED(0,2,"STC-开源遥控");
	GB2312_OLED(0,3,"稳定版本v2.5");
}
/*------------------------------------------------
  OLED菜单选项函数 
------------------------------------------------*/
void OLEDCaiDanA(void)
{
	//此功能未设计（后期开发v3.0）
	GB2312_OLED(0,0,"飞控控制模式");
	GB2312_OLED(0,1,"未开发");
//	GB2312_OLED(0,2,"");
	GB2312_OLED(0,3,"后期版本更新");
}
/*------------------------------------------------
  OLED菜单选项函数 
------------------------------------------------*/
void OLEDCaiDanB(void)
{
	//此功能未设计（后期开发v3.0）
	GB2312_OLED(0,0,"飞控硬件检测");
	GB2312_OLED(0,1,"未开发");
//	GB2312_OLED(0,2,"");
	GB2312_OLED(0,3,"后期版本更新");
}
/*------------------------------------------------
  OLED手动校准函数 汉字界面
------------------------------------------------*/
void OLEDXiaoZun(void)
{
GB2312_OLED(0,0,"手动校准微调");
GB2312_OLED(0,1,"航向:");
GB2312_OLED(0,2,"横滚:");
GB2312_OLED(0,3,"俯仰:");
}
/*------------------------------------------------
  OLED手动微调参数
------------------------------------------------*/
void OLEDSDCanShuA(unsigned char S1,unsigned char S2,unsigned char S3)
{ unsigned char SM1,SM2,SM3;
	if(S1>128){SM1=S1-128;OLED_shuzi(6,1,20);}//+号
	else      {SM1=128-S1;OLED_shuzi(6,1,21);}//-号
	if(S2>128){SM2=S2-128;OLED_shuzi(6,2,20);}//+号
	else      {SM2=128-S2;OLED_shuzi(6,2,21);}//-号
	if(S3>128){SM3=S3-128;OLED_shuzi(6,3,20);}//+号
	else      {SM3=128-S3;OLED_shuzi(6,3,21);}//-号
	//显示微调值 3位整数
	OLED_shuzi(8,1,SM1%1000/100);
	OLED_shuzi(9,1,SM1%100/10);
	OLED_shuzi(10,1,SM1%10%10);
	OLED_shuzi(8,2,SM2%1000/100);
	OLED_shuzi(9,2,SM2%100/10);
	OLED_shuzi(10,2,SM2%10%10);
	OLED_shuzi(8,3,SM3%1000/100);
	OLED_shuzi(9,3,SM3%100/10);
	OLED_shuzi(10,3,SM3%10%10);
}
/*------------------------------------------------
  OLED界面函数 飞控飞行界面
------------------------------------------------*/
void OLEDJieMian(void)
{
	GB2312_OLED(0,0,"油门:");
	GB2312_OLED(0,1,"航向:");
	GB2312_OLED(0,2,"横滚:");
	GB2312_OLED(0,3,"俯仰:");

//	OLED_shuzi(11,1,25);OLED_shuzi(11,2,26);//XY
//	OLED_shuzi(15,1,19);OLED_shuzi(15,2,19);//度数符号
}
/*------------------------------------------------
 OLED参数显示函数1 手动微调值
------------------------------------------------*/
void OLEDCanShuA(unsigned int A1,unsigned char A2,unsigned char A3,unsigned char A4)
{
	//显示摇杆数据值 
	OLED_shuzi(5,0,A1%10000/1000);
	OLED_shuzi(6,0,A1%1000/100);
	OLED_shuzi(7,0,A1%100/10);
	OLED_shuzi(8,0,A1%10%10);
	OLED_shuzi(5,1,A2%1000/100);
	OLED_shuzi(6,1,A2%100/10);
	OLED_shuzi(7,1,A2%10%10);
	OLED_shuzi(5,2,A3%1000/100);
	OLED_shuzi(6,2,A3%100/10);
	OLED_shuzi(7,2,A3%10%10);
	OLED_shuzi(5,3,A4%1000/100);
	OLED_shuzi(6,3,A4%100/10);
	OLED_shuzi(7,3,A4%10%10);
}
/*------------------------------------------------
  OLED参数显示函数2	飞控飞行数据值
------------------------------------------------*/
void OLEDCanShuB(unsigned char A1,unsigned char A2,unsigned char A3)
{ unsigned char AM1,AM2;
if(A1>128){AM1=A1-128;OLED_shuzi(9,1,20);}//+号
else      {AM1=128-A1;OLED_shuzi(9,1,21);}//-号
if(A2>128){AM2=A2-128;OLED_shuzi(9,2,20);}//+号
else      {AM2=128-A2;OLED_shuzi(9,2,21);}//-号
//显示飞控角度值
OLED_shuzi(10,1,AM1%100/10);
OLED_shuzi(11,1,AM1%10%10);
OLED_shuzi(10,2,AM2%100/10);
OLED_shuzi(11,2,AM2%10%10);

A3=A3; //无用
/*显示飞控电压（后期开发v3.0）
OLED_shuzi(11,3,A3%1000/100);
OLED_shuzi(12,3,17);//"."
OLED_shuzi(13,3,A3%100/10);
OLED_shuzi(14,3,A3%10%10);
OLED_shuzi(15,3,22);//"V"
*/
} 
/*------------------------------------------------
 //所有I/O口全设为准双向，弱上拉模式           
------------------------------------------------*/
void GPIOINT(void)
{
	P0M0=0x00;P0M1=0x00;
	P1M0=0x00;P1M1=0x00;
	P2M0=0x00;P2M1=0x00;
	P3M0=0x00;P3M1=0x00;
	P4M0=0x00;P4M1=0x00;
	P5M0=0x00;P5M1=0x00;
	P6M0=0x00;P6M1=0x00;
	P7M0=0x00;P7M1=0x00;
}
/*------------------------------------------------
                  主函数
------------------------------------------------*/
void main(void)
{	
	unsigned int  AD1,ADC1;
	unsigned char AD2,AD3,AD4;//读取当前ADC变量
	unsigned char ADC2,ADC3,ADC4;//优化后控制变量
	unsigned char SZML;//四轴命令状态控制变量
	unsigned char SSLL;//通讯状态 变量
	unsigned char TXGX;//飞控数据更新 变量
	unsigned char SZ;  //手动微调 变量	
	unsigned char HGJD,FYJD,FKDY;//飞控姿态电压变量
	unsigned char HangXiang,HengGun,FuYang;//微调变量

	GPIOINT();			//GOIO口 初始化
	Delay(10);			//延时一会
	InitADC();			//ADC初始化
	Delay(10);			//延时一会
	init_NRF24L01();	//NRF24L01 初始化
	nRF24L01_RX_Mode(RxBuf);//数据接收
	Delay(10);			//延时一会
	ZiKuIC_init();      //ZiKuIC_init初始化
   	OLED_SSD1305Init();	//OLED初始化
    OLED_CLS();		   	//OLED复位清屏
	Delay(10);			//延时一会
	OLEDKaiJi();		//OLED开机问候界面
//---------------------------------------//
	SSLL=0;//失联变量 初始化
	SZML=0;//命令变量 初始化
  while(1)
	{
//--------------------------------------------------------------------//
	if(KS1==0){LEDA=0;LEDR=1;LEDG=1;LEDB=1;SZML=1;//A键 待机状态
	OLED_CLS();OLEDCaiDanA();Delay(100);while(KS1==0);}
	if(KS2==0){LEDA=1;LEDR=1;LEDG=0;LEDB=1;SZML=2;//B键 飞控波形
	OLED_CLS();OLEDCaiDanB();Delay(100);while(KS2==0);}
	if(KS3==0){LEDA=1;LEDR=0;LEDG=1;LEDB=1;SZML=3;TXGX=1;//C键 修改参数
	OLED_CLS();OLEDXiaoZun();Delay(100);while(KS3==0);}//
	if(KS4==0){LEDA=1;LEDR=1;LEDG=1;LEDB=0;SZML=5;//D键 解锁飞行
	OLED_CLS();OLEDJieMian();Delay(100);while(KS4==0);}//
//--------------------------------------------------------------------//
	if(KAL==0){TXGX=0x0A;Feng=0;Delay(100);while(KAL==0);Feng=1;} //L键 启动飞控 一键校准功能
//--------------------------------------------------------------------//
	if(SZML==3){ // 手动微调 修改参数
		if(KS5==0){SZ--;Delay(100);while(KS5==0);}//修改目标值 括号上移
		if(KS6==0){SZ++;Delay(100);while(KS6==0);}//修改目标值 括号下移
		if(SZ<1)SZ=3;if(SZ>3)SZ=1;				  //修改数据数量 限制：3个
		if(SZ==1){OLED_shuzi(5,1,23);OLED_shuzi(11,1,24);//显示 修改航向值
				  if(KS7==0){HangXiang--;Delay(100);while(KS7==0);TXGX=2;}
				  if(KS8==0){HangXiang++;Delay(100);while(KS8==0);TXGX=2;}}
		else     {OLED_shuzi(5,1,16);OLED_shuzi(11,1,16);} 
		if(SZ==2){OLED_shuzi(5,2,23);OLED_shuzi(11,2,24);//显示 修改横滚值
				  if(KS7==0){HengGun--;Delay(100);while(KS7==0);TXGX=2;}
				  if(KS8==0){HengGun++;Delay(100);while(KS8==0);TXGX=2;}}
		else     {OLED_shuzi(5,2,16);OLED_shuzi(11,2,16);} 
		if(SZ==3){OLED_shuzi(5,3,23);OLED_shuzi(11,3,24);//显示 修改俯仰值
				  if(KS7==0){FuYang--;Delay(100);while(KS7==0);TXGX=2;}
				  if(KS8==0){FuYang++;Delay(100);while(KS8==0);TXGX=2;}}
		else     {OLED_shuzi(5,3,16);OLED_shuzi(11,3,16);} 

		OLEDSDCanShuA(HangXiang,HengGun,FuYang);//OLED显示手动微调参数
				}
//--------------------------------------------------------------------//
	if(SZML==5){
		AD1=GetADCResult(0); //油门
		AD2=GetADCResult(1)/4; //航向
		AD3=GetADCResult(3)/4; //横滚
		AD4=GetADCResult(2)/4; //俯仰
		
		//油门参数
		ADC1=AD1;
		//航向参数
		if(AD2>133)			ADC2=128+(AD2-133)/2;
		else if(AD2<123)	ADC2=128-(123-AD2)/2;
	 	else  ADC2=128;		ADC2=ADC2;
		//横滚参数
		if(AD3>138)			ADC3=133+(AD3-138)/2;
		else if(AD3<124)	ADC3=133-(124-AD3)/2;
		else  ADC3=133;		ADC3=ADC3-5;
		//俯仰参数
		if(AD4>138)			ADC4=133+(AD4-138)/2;
		else if(AD4<128)	ADC4=133-(128-AD4)/2;
		else  ADC4=133;		ADC4=ADC4-5;
		}
	if(SZML==5)//刷新当前变量
		{
		OLEDCanShuA(ADC1,ADC2,ADC3,ADC4);//显示摇杆参数
		OLEDCanShuB(HGJD,FYJD,FKDY);//显示飞控参数
		}

	TxBuf[0]=SSLL++;//发送 失联变量
	TxBuf[1]=SZML;//发送 命令值
	TxBuf[2]=ADC1/0xff;//发送 油门参数 高2位 
	TxBuf[3]=ADC1%0xff;//发送 油门参数 低8位
	TxBuf[4]=ADC2;//发送 航向值
	TxBuf[5]=ADC3;//发送 横滚值
	TxBuf[6]=ADC4;//发送 俯仰值
	if(TXGX==1){TXGX=0;TxBuf[7]=1;}//发送 飞控数据更新请求
	if(TXGX==2){TXGX=0;TxBuf[7]=3; //发送 手动微调值
	TxBuf[8]=HengGun;	//发送 横滚微调变量
	TxBuf[9]=FuYang;  	//发送 俯仰微调变量
	TxBuf[10]=HangXiang;	//发送 航向微调变量
	}
	if(TXGX==0X0A){TXGX=0;TxBuf[7]=0x0A;}//发送 命令飞控 自动校准
	nRF24L01_TX_Mode(TxBuf);//加载目标地址 数据发送
	Delay(5);
	nRF24L01_RX_Mode(RxBuf);//加载自己地址 数据接收
	Delay(5);
//	SSLL=RxBuf[0];//接收 失联变量
//	    =RxBuf[1];//接收 状态变量
	HGJD=RxBuf[2];//接收 横滚角度变量
	FYJD=RxBuf[3];//接收 俯仰角度变量
	FKDY=RxBuf[4];//接收 电压参数变量
//	    =RxBuf[5];//接收
//	    =RxBuf[6];//接收
	if(RxBuf[7]==2){RxBuf[7]=0;TxBuf[7]=0;//复位结果值
	HengGun	 =RxBuf[8];//接收 横滚微调变量
	FuYang	 =RxBuf[9];//接收 俯仰微调变量
	HangXiang=RxBuf[10];//接收 航向微调变量
	}
	if(RxBuf[7]==4){RxBuf[7]=0;TxBuf[7]=0;}//复位结果值(判断执行参数修改成功)
	if(RxBuf[7]==0xA1){RxBuf[7]=0;TxBuf[7]=0;}//复位结果值(判断执行参数修改成功)
  }	
}
/*
	if(KAL==0)LEDA=0;else LEDA=1;//左键LED亮
	if(KAR==0)Feng=0;else Feng=1;//右键蜂鸣器响
	     
		 if(KS1==0){LEDR=0;LEDG=1;LEDB=1;} //按键A
	else if(KS2==0){LEDR=1;LEDG=0;LEDB=1;} //按键B
	else if(KS3==0){LEDR=1;LEDG=1;LEDB=0;} //按键C
	else if(KS4==0){LEDR=0;LEDG=0;LEDB=0;} //按键D

	else if(KS5==0){LEDR=0;LEDG=0;LEDB=0;} //按键上
	else if(KS6==0){LEDR=0;LEDG=0;LEDB=0;} //按键下
	else if(KS7==0){LEDR=0;LEDG=0;LEDB=0;} //按键左
	else if(KS8==0){LEDR=0;LEDG=0;LEDB=0;} //按键右

	else           {LEDR=1;LEDG=1;LEDB=1;} //关闭全彩LED
*/
