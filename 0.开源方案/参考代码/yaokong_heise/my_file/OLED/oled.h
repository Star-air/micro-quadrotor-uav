#ifndef _oled
#define _oled
#include "sys.h"
#include "stdlib.h"	

#define OLED_SCLK_Clr() GPIO_ResetBits(GPIOB,GPIO_Pin_6)//SCL
#define OLED_SCLK_Set() GPIO_SetBits(GPIOB,GPIO_Pin_6)

#define OLED_SDIN_Clr() GPIO_ResetBits(GPIOB,GPIO_Pin_7)//SDA
#define OLED_SDIN_Set() GPIO_SetBits(GPIOB,GPIO_Pin_7)

#define OLED_MODE 0
#define SIZE 8
#define XLevelL		0x00
#define XLevelH		0x10
#define Max_Column	128
#define Max_Row		64
#define	Brightness	0xFF 
#define X_WIDTH 	128
#define Y_WIDTH 	64	    

#define OLED_CMD  0	//写命令
#define OLED_DATA 1	//写数据

//OLED控制用函数
void OLED_WR_Byte(unsigned dat,unsigned cmd);  
void OLED_Display_On(void);
void OLED_Display_Off(void);	   							   		    
void OLED_Init(void); //初始化
void OLED_Clear(void);  //清屏
void OLED_DrawPoint(u8 x,u8 y,u8 t);
void OLED_ShowChar(u8 x,u8 y,u8 chr,u8 Char_Size);  //显示单个字符
void OLED_ShowNum(u8 x,u8 y,s32 num,u8 len,u8 size);  //显示数字
void OLED_ShowString(u8 x,u8 y, u8 *p,u8 Char_Size);	 //显示字符串
void OLED_Set_Pos(unsigned char x, unsigned char y);  //显示坐标
void OLED_ShowCHinese(u8 x,u8 y,u8 no);  //显示中文
void OLED_ShowFloat(u8 x,u8 y,float num,u8 len,u8 len_,u8 size);
void IIC_Start(void);
void IIC_Stop(void);
void Write_IIC_Command(unsigned char IIC_Command);
void Write_IIC_Data(unsigned char IIC_Data);
void Write_IIC_Byte(unsigned char IIC_Byte);

void IIC_Wait_Ack(void);




#endif



