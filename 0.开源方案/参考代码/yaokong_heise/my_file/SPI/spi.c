#include "spi.h"

int spi_init(void)
{
	SPI_InitTypeDef SPI_Initstrcut;
	GPIO_InitTypeDef GPIO_Initstruct_spi;
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1|RCC_APB2Periph_GPIOA|RCC_APB2Periph_GPIOB,ENABLE);
	//初始化SPI传输引脚   PB5，PB6，PB7
	GPIO_Initstruct_spi.GPIO_Mode=GPIO_Mode_AF_PP;    //复用推挽输出
	GPIO_Initstruct_spi.GPIO_Pin=GPIO_Pin_5|GPIO_Pin_6|GPIO_Pin_7;//第6.7.5引脚
	GPIO_Initstruct_spi.GPIO_Speed=GPIO_Speed_50MHz;    //输出频率50m
	GPIO_Init(GPIOA,&GPIO_Initstruct_spi);
	GPIO_SetBits(GPIOA,GPIO_Pin_5|GPIO_Pin_6|GPIO_Pin_7);  //PB13/14/15上拉
	//初始化 NRF24L01 CE(a4)
	GPIO_Initstruct_spi.GPIO_Mode=GPIO_Mode_Out_PP;//推挽输出
	GPIO_Initstruct_spi.GPIO_Pin=GPIO_Pin_4;
	GPIO_Init(GPIOA,&GPIO_Initstruct_spi);
	//初始化 NRF24L01 CS(b10)
	GPIO_Initstruct_spi.GPIO_Mode=GPIO_Mode_Out_PP;//推挽输出
	GPIO_Initstruct_spi.GPIO_Pin=GPIO_Pin_10;
	GPIO_Init(GPIOB,&GPIO_Initstruct_spi);
	//初始化 NRF24L01 IRQ(b0)
	GPIO_Initstruct_spi.GPIO_Mode=GPIO_Mode_IPD;//输入
	GPIO_Initstruct_spi.GPIO_Pin=GPIO_Pin_0;
	GPIO_Init(GPIOB,&GPIO_Initstruct_spi);
	
	SPI_Initstrcut.SPI_BaudRatePrescaler= SPI_BaudRatePrescaler_16;   //波特率预分频值为256
	SPI_Initstrcut.SPI_CPHA=SPI_CPHA_1Edge;    
	SPI_Initstrcut.SPI_CPOL=SPI_CPOL_Low;
	SPI_Initstrcut.SPI_CRCPolynomial=7;
	SPI_Initstrcut.SPI_DataSize=SPI_DataSize_8b;   //八位数据   可设16位
	SPI_Initstrcut.SPI_Direction=SPI_Direction_2Lines_FullDuplex;
	SPI_Initstrcut.SPI_FirstBit=SPI_FirstBit_MSB;
	SPI_Initstrcut.SPI_Mode=SPI_Mode_Master;   //SPI主机
	SPI_Initstrcut.SPI_NSS=SPI_NSS_Soft;   //nrf nss信号由软件控制
	SPI_Init(SPI1,&SPI_Initstrcut);
	SPI_Cmd(SPI1, ENABLE); //使能SPI外设
	
	return 0;
	
}

//SPI 速度设置函数
//SpeedSet:
//SPI_BaudRatePrescaler_2   2分频   
//SPI_BaudRatePrescaler_8   8分频   
//SPI_BaudRatePrescaler_16  16分频  
//SPI_BaudRatePrescaler_256 256分频 
  
void SPI2_SetSpeed(u8 SPI_BaudRatePrescaler)
{
  	assert_param(IS_SPI_BAUDRATE_PRESCALER(SPI_BaudRatePrescaler));
	SPI1->CR1&=0XFFC7;
	SPI1->CR1|=SPI_BaudRatePrescaler;	//设置SPI2速度 
	SPI_Cmd(SPI1,ENABLE);

} 

//SPIx 读写一个字节
//TxData:要写入的字节
//返回值:读取到的字节
u8 SPI2_ReadWriteByte(u8 TxData)
{		
	u8 retry=0;				 	
	while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE) == RESET) //检查指定的SPI标志位设置与否:发送缓存空标志位
		{
		retry++;
		if(retry>200)return 0;
		}			  
	SPI_I2S_SendData(SPI1, TxData); //通过外设SPIx发送一个数据
	retry=0;

	while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE) == RESET)//检查指定的SPI标志位设置与否:接受缓存非空标志位
		{
		retry++;
		if(retry>200)return 0;
		}	  						    
	return SPI_I2S_ReceiveData(SPI1); //返回通过SPIx最近接收的数据					    
}

