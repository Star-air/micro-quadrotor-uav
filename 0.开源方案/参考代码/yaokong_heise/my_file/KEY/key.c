#include "key.h"
#include "delay.h"
#include "led.h"
#include "oled.h"
#include "control.h"
#include "time.h"
#include "nrf.h"
//按键参数
float buchang=1;
key_type key;
pid_type pid;
//起飞标志
extern u8 nrf_send[6];
//通用参数
extern tongyong_type tongyong;
void key_init(void)
{
	GPIO_InitTypeDef GPIO_InitStruct_key;
	EXTI_InitTypeDef EXTI_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	//使能gpio时钟
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC,ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO,ENABLE);	//使能复用功能时钟
	//初始化gpio
	GPIO_InitStruct_key.GPIO_Mode= GPIO_Mode_IPU; //推挽输出
	GPIO_InitStruct_key.GPIO_Pin=GPIO_Pin_2|GPIO_Pin_13|GPIO_Pin_14|GPIO_Pin_15;
	GPIO_InitStruct_key.GPIO_Speed=GPIO_Speed_50MHz;  
	GPIO_Init(GPIOC,&GPIO_InitStruct_key);
	
	GPIO_EXTILineConfig(GPIO_PortSourceGPIOC,GPIO_PinSource2);
	EXTI_InitStructure.EXTI_Line=EXTI_Line2;	//KEY2
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;	
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStructure);	 	//根据EXTI_InitStruct中指定的参数初始化外设EXTI寄存器
	
	GPIO_EXTILineConfig(GPIO_PortSourceGPIOC,GPIO_PinSource13);
  EXTI_InitStructure.EXTI_Line=EXTI_Line13;
  EXTI_Init(&EXTI_InitStructure);	  	//根据EXTI_InitStruct中指定的参数初始化外设EXTI寄存器
	
	GPIO_EXTILineConfig(GPIO_PortSourceGPIOC,GPIO_PinSource14);
  EXTI_InitStructure.EXTI_Line=EXTI_Line14;
  EXTI_Init(&EXTI_InitStructure);	  	//根据EXTI_InitStruct中指定的参数初始化外设EXTI寄存器
	
	GPIO_EXTILineConfig(GPIO_PortSourceGPIOC,GPIO_PinSource15);
  EXTI_InitStructure.EXTI_Line=EXTI_Line15;
  EXTI_Init(&EXTI_InitStructure);	  	//根据EXTI_InitStruct中指定的参数初始化外设EXTI寄存器
	
	
	NVIC_InitStructure.NVIC_IRQChannel = EXTI2_IRQn;			//使能按键WK_UP所在的外部中断通道
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x02;	//抢占优先级2， 
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x03;					//子优先级3
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;								//使能外部中断通道
	NVIC_Init(&NVIC_InitStructure);
	
	NVIC_InitStructure.NVIC_IRQChannel = EXTI15_10_IRQn;			//使能按键WK_UP所在的外部中断通道
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x02;	//抢占优先级2， 
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x03;					//子优先级3
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;								//使能外部中断通道
	NVIC_Init(&NVIC_InitStructure);
	
	
		
		key.zhu_weizhi=2;   //主菜单显示图标位置
		key.canshu_weihzi=0;
		key.canshu_jin=0;
		key.buchang_jin=0;
		key.buchang_weizhi=2;
		pid.P_wai_kp=0;  //左右方向的内外环PID参数
	  pid.P_wai_ki=0;
	  pid.P_nei_kp=0;
	  pid.P_nei_ki=0;
	  pid.P_nei_kd=0;
	
	  pid.R_wai_kp=0;  //前后方向的内外环PID参数
 	  pid.R_wai_ki=0;
	  pid.R_nei_kp=0;
	  pid.R_nei_ki=0;
	  pid.R_nei_kd=0;
	 //旋转量
	  pid.Y_kp=0;
	  pid.Y_kd=0;
	
	
	
}

/*
显示主菜单
*/
void xianshi_zhu(void)
{
	OLED_ShowString(0,0,"Y:",12);
	OLED_ShowFloat(13,0,tongyong.dianya,3,2,12);
	OLED_ShowString(60,0,"J:",12);
	OLED_ShowNum(73,0,tongyong.dianya_jishen/100,1,12);
	OLED_ShowString(79,0,".",12);
	OLED_ShowNum(85,0,tongyong.dianya_jishen%100,2,12);
	OLED_ShowString(2,key.zhu_weizhi,">",16);
	OLED_ShowString(18,2,"CANHUS",16);
	OLED_ShowString(18,4,"BUCHANG",16);
	OLED_ShowString(18,6,"FLY",16);
	//OLED_ShowNum(58,1,tongyong.speed,1,12);
	
}
extern u8 jishen_error;
void fly_menu(void)
{
	if(jishen_error==0)
	{
		//进入飞行状态
		OLED_ShowCHinese(21,0,0);  //上
		OLED_ShowCHinese(21,4,1);  //下
		OLED_ShowCHinese(5,2,2);   //左
		OLED_ShowCHinese(37,2,3);   //右
		
		OLED_ShowCHinese(85,0,0);  //上
		OLED_ShowCHinese(85,4,1);  //下
		OLED_ShowCHinese(69,2,2);   //左
		OLED_ShowCHinese(101,2,3);   //右
		nrf_send[1]=1;  //可以起飞
		//EXTI_DeInit();
		time3_init(10);
		
	}
	else
	{
		OLED_ShowString(5,2,"ERROR IS SAVE!",16);
		OLED_ShowString(5,4,"PLEASE CHECK!",16);
	}
	if(key.key_value==4)
	{
		key.zhu_jin=0;
		nrf_send[1]=0;  // 不可以起飞（瞬间降落）
		OLED_Clear();
		xianshi_zhu();
	}
}
void canshu_menu(void)
{
	OLED_ShowString(0,0,">",12);
	
	OLED_ShowString(7,0,"pwp:",12);//32
	OLED_ShowString(7,1,"pwi:",12);
	OLED_ShowString(7,2,"pnp:",12);
	OLED_ShowString(7,3,"pni:",12);
	OLED_ShowString(7,4,"pnd:",12);
	
	OLED_ShowString(71,0,"rwp:",12);
	OLED_ShowString(71,1,"rwi:",12);
	OLED_ShowString(71,2,"rnp:",12);
	OLED_ShowString(71,3,"rni:",12);
	OLED_ShowString(71,4,"rnd:",12);
	
	OLED_ShowString(7,5,"p:",12);
	OLED_ShowString(71,5,"d:",12);
	
	OLED_ShowNum(38,0,pid.P_wai_kp,3,12);
	OLED_ShowNum(38,1,pid.P_wai_ki,3,12);
	OLED_ShowNum(38,2,pid.P_nei_kp,3,12);
	OLED_ShowNum(38,3,pid.P_nei_ki,3,12);
	OLED_ShowNum(38,4,pid.P_nei_kd,3,12);
	
	OLED_ShowNum(102,0,pid.R_wai_kp,3,12);
	OLED_ShowNum(102,1,pid.R_wai_ki,3,12);
	OLED_ShowNum(102,2,pid.R_nei_kp,3,12);
	OLED_ShowNum(102,3,pid.R_nei_ki,3,12);
	OLED_ShowNum(102,4,pid.R_nei_kd,3,12);
	
	OLED_ShowNum(38,5,pid.Y_kp,3,12);
	OLED_ShowNum(102,5,pid.Y_kd,3,12);
	
	
}
void canshu_xianshi(void)
{
	OLED_ShowString(0,0," ",12);
	OLED_ShowString(0,1," ",12);
	OLED_ShowString(0,2," ",12);
	OLED_ShowString(0,3," ",12);
	OLED_ShowString(0,4," ",12);
	OLED_ShowString(0,5," ",12);
	
	OLED_ShowString(64,0," ",12);
	OLED_ShowString(64,1," ",12);
	OLED_ShowString(64,2," ",12);
	OLED_ShowString(64,3," ",12);
	OLED_ShowString(64,4," ",12);
	OLED_ShowString(64,5," ",12);
	
	if(key.canshu_weihzi<6)
	{
		OLED_ShowString(0,key.canshu_weihzi,">",12);
	}
	else
	{
		OLED_ShowString(64,key.canshu_weihzi-6,">",12);
	}
}
void canshu_debug(void)
{
	switch(key.canshu_jin)
	{
		case 0:
			switch(key.key_value)
			{
				case 1:
					key.canshu_weihzi--;
				if(key.canshu_weihzi<0)key.canshu_weihzi=11;
				canshu_xianshi();
					break;
				case 2:
					key.canshu_weihzi++;
				if(key.canshu_weihzi>11)key.canshu_weihzi=0;
				canshu_xianshi();
					break;
				case 3:
					key.canshu_jin=1;
					if(key.canshu_weihzi<6)
					{
						OLED_ShowString(0,key.canshu_weihzi,"<",12);
					}
					else
					{
						OLED_ShowString(64,key.canshu_weihzi-6,"<",12);
					}
					break;
				case 4:
					key.zhu_jin=0;
					OLED_Clear();
					xianshi_zhu();
					break;
			}
			break;
		case 1:  //进入参数修改
			switch(key.canshu_weihzi)
			{
				case 0:
					switch(key.key_value)
					{
						case 1:
							nrf_send[2]=2;
							break;
						case 2:
							nrf_send[2]=1;
							break;
						case 3:
							//fasong 成功后显示
							nrf_send[1]=2;
							nrf_send[2]=0;
						  if(NRF24L01_TxPacket(nrf_send)==TX_OK)
							{
								LED1=!LED1;
								if(nrf_send[2]==1)
									OLED_ShowNum(38,0,--pid.P_wai_kp,3,12);
								else
									OLED_ShowNum(38,0,++pid.P_wai_kp,3,12);
							}
							break;
						case 4:
							key.canshu_jin=0;
							if(key.canshu_weihzi<6)
							{
								OLED_ShowString(0,key.canshu_weihzi,">",12);
							}
							else
							{
								OLED_ShowString(64,key.canshu_weihzi-6,">",12);
							}
							break;
					}
					break;
				case 1:
					switch(key.key_value)
					{
						case 1:
							nrf_send[2]=2;
							break;
						case 2:
							nrf_send[2]=1;
							break;
						case 3:
							//fasong 成功后显示
							nrf_send[1]=2;
							nrf_send[2]=1;
						  if(NRF24L01_TxPacket(nrf_send)==TX_OK)
							{
								LED1=!LED1;
								if(nrf_send[2]==1)
								OLED_ShowNum(38,1,--pid.P_wai_ki,3,12);
								else
                OLED_ShowNum(38,1,++pid.P_wai_ki,3,12);
							}
							break;
						case 4:
							key.canshu_jin=0;
							if(key.canshu_weihzi<6)
							{
								OLED_ShowString(0,key.canshu_weihzi,">",12);
							}
							else
							{
								OLED_ShowString(64,key.canshu_weihzi-6,">",12);
							}
							break;
					}
					break;
				case 2:
					switch(key.key_value)
					{
						case 1:
							nrf_send[2]=2;
							break;
						case 2:
							nrf_send[2]=1;
							break;
						case 3:
							//fasong 成功后显示
							nrf_send[1]=2;
							nrf_send[2]=2;
							if(NRF24L01_TxPacket(nrf_send)==TX_OK)
							{
								LED1=!LED1;
									if(nrf_send[2]==1)
										OLED_ShowNum(38,2,--pid.P_nei_kp,3,12);

									else
										OLED_ShowNum(38,2,++pid.P_nei_kp,3,12);
							}
							break;
						case 4:
							key.canshu_jin=0;
							if(key.canshu_weihzi<6)
							{
								OLED_ShowString(0,key.canshu_weihzi,">",12);
							}
							else
							{
								OLED_ShowString(64,key.canshu_weihzi-6,">",12);
							}
							break;
					}
					break;
				case 3:
					switch(key.key_value)
					{
						case 1:
							nrf_send[2]=2;
							break;
						case 2:
							nrf_send[2]=1;
							break;
						case 3:
							//fasong 成功后显示
							nrf_send[1]=2;
							nrf_send[2]=3;
						  if(NRF24L01_TxPacket(nrf_send)==TX_OK)
							{
								LED1=!LED1;
								  if(nrf_send[2]==1)
										OLED_ShowNum(38,3,--pid.P_nei_ki,3,12);

									else
										OLED_ShowNum(38,3,++pid.P_nei_ki,3,12);
							}
							break;
							
						case 4:
							key.canshu_jin=0;
							if(key.canshu_weihzi<6)
							{
								OLED_ShowString(0,key.canshu_weihzi,">",12);
							}
							else
							{
								OLED_ShowString(64,key.canshu_weihzi-6,">",12);
							}
							break;
					}
					break;
				case 4:
					switch(key.key_value)
					{
						case 1:
							nrf_send[2]=2;
							break;
						case 2:
							nrf_send[2]=1;
							break;
						case 3:
							//fasong 成功后显示
							nrf_send[1]=2;
							nrf_send[2]=4;
						if(NRF24L01_TxPacket(nrf_send)==TX_OK)
							{
								LED1=!LED1;
								 if(nrf_send[2]==1)
											OLED_ShowNum(38,4,--pid.P_nei_kd,3,12);

										else
											OLED_ShowNum(38,4,++pid.P_nei_kd,3,12);
							}
							break;
						case 4:
							key.canshu_jin=0;
							if(key.canshu_weihzi<6)
							{
								OLED_ShowString(0,key.canshu_weihzi,">",12);
							}
							else
							{
								OLED_ShowString(64,key.canshu_weihzi-6,">",12);
							}
							break;
					}
					break;
				case 5:
					switch(key.key_value)
					{
						case 1:
							nrf_send[2]=2;
							break;
						case 2:
							nrf_send[2]=1;
							break;
						case 3:
							//fasong 成功后显示
							nrf_send[1]=2;
							nrf_send[2]=10;
						if(NRF24L01_TxPacket(nrf_send)==TX_OK)
							{
								LED1=!LED1;
									if(nrf_send[2]==1)
											OLED_ShowNum(38,5,--pid.Y_kp,3,12);

										else
											OLED_ShowNum(38,5,++pid.Y_kp,3,12);
							}
								break;
						case 4:
							key.canshu_jin=0;
							if(key.canshu_weihzi<6)
							{
								OLED_ShowString(0,key.canshu_weihzi,">",12);
							}
							else
							{
								OLED_ShowString(64,key.canshu_weihzi-6,">",12);
							}
							break;
					}
					break;
				case 6:
					switch(key.key_value)
					{
						case 1:
							nrf_send[2]=2;
							break;
						case 2:
							nrf_send[2]=1;
							break;
						case 3:
							//fasong 成功后显示
						 nrf_send[1]=2;
							nrf_send[2]=5;
						if(NRF24L01_TxPacket(nrf_send)==TX_OK)
							{  
								LED1=!LED1;
								if(nrf_send[2]==1)
											OLED_ShowNum(102,0,--pid.R_wai_kp,3,12);

										else
											OLED_ShowNum(102,0,++pid.R_wai_kp,3,12);
									}
								break;
						case 4:
							key.canshu_jin=0;
							if(key.canshu_weihzi<6)
							{
								OLED_ShowString(0,key.canshu_weihzi,">",12);
							}
							else
							{
								OLED_ShowString(64,key.canshu_weihzi-6,">",12);
							}
							break;
					}
				case 7:
					switch(key.key_value)
					{
						case 1:
							nrf_send[2]=2;
							break;
						case 2:
							nrf_send[2]=1;
							break;
						case 3:
							//fasong 成功后显示
							nrf_send[1]=2;
							nrf_send[2]=6;
						if(NRF24L01_TxPacket(nrf_send)==TX_OK)
							{  
								LED1=!LED1;
								if(nrf_send[2]==1)
											OLED_ShowNum(102,1,--pid.R_wai_ki,3,12);

										else
											OLED_ShowNum(102,1,++pid.R_wai_ki,3,12);
									}
								break;
						case 4:
							key.canshu_jin=0;
							if(key.canshu_weihzi<6)
							{
								OLED_ShowString(0,key.canshu_weihzi,">",12);
							}
							else
							{
								OLED_ShowString(64,key.canshu_weihzi-6,">",12);
							}
							break;
					}
					break;
				case 8:
					switch(key.key_value)
					{
						case 1:
							nrf_send[2]=2;
							break;
						case 2:
							nrf_send[2]=1;
							break;
						case 3:
							//fasong 成功后显示
						  nrf_send[1]=2;
							nrf_send[2]=7;
						if(NRF24L01_TxPacket(nrf_send)==TX_OK)
							{  
								LED1=!LED1;
								if(nrf_send[2]==1)
											OLED_ShowNum(102,2,--pid.R_nei_kp,3,12);

										else
											OLED_ShowNum(102,2,++pid.R_nei_kp,3,12);
									}
								break;
						case 4:
							key.canshu_jin=0;
							if(key.canshu_weihzi<6)
							{
								OLED_ShowString(0,key.canshu_weihzi,">",12);
							}
							else
							{
								OLED_ShowString(64,key.canshu_weihzi-6,">",12);
							}
							break;
					}
					break;
				case 9:
					switch(key.key_value)
					{
						case 1:
							nrf_send[2]=2;
							break;
						case 2:
							nrf_send[2]=1;
							break;
						case 3:
							//fasong 成功后显示
							nrf_send[1]=2;
							nrf_send[2]=8;
						if(NRF24L01_TxPacket(nrf_send)==TX_OK)
							{  
								LED1=!LED1;
								if(nrf_send[2]==1)
											OLED_ShowNum(102,3,--pid.R_nei_ki,3,12);

										else
											OLED_ShowNum(102,3,++pid.R_nei_ki,3,12);
									}
								break;
						case 4:
							key.canshu_jin=0;
							if(key.canshu_weihzi<6)
							{
								OLED_ShowString(0,key.canshu_weihzi,">",12);
							}
							else
							{
								OLED_ShowString(64,key.canshu_weihzi-6,">",12);
							}
							break;
					}
					break;
				case 10:
					switch(key.key_value)
					{
						case 1:
							nrf_send[2]=2;
							break;
						case 2:
							nrf_send[2]=1;
							break;
						case 3:
							//fasong 成功后显示
						  nrf_send[1]=2;
							nrf_send[2]=9;
						if(NRF24L01_TxPacket(nrf_send)==TX_OK)
							{  
								LED1=!LED1;
								if(nrf_send[2]==1)
											OLED_ShowNum(102,4,--pid.R_nei_kd,3,12);

										else
											OLED_ShowNum(102,4,++pid.R_nei_kd,3,12);
									}
								break;
						case 4:
							key.canshu_jin=0;
							if(key.canshu_weihzi<6)
							{
								OLED_ShowString(0,key.canshu_weihzi,">",12);
							}
							else
							{
								OLED_ShowString(64,key.canshu_weihzi-6,">",12);
							}
							break;
					}
					break;
				case 11:
					switch(key.key_value)
					{
						case 1:
							nrf_send[2]=2;
							break;
						case 2:
							nrf_send[2]=1;
							break;
						case 3:
							//fasong 成功后显示
							nrf_send[1]=2;
							nrf_send[2]=11;
						if(NRF24L01_TxPacket(nrf_send)==TX_OK)
							{  
								LED1=!LED1;
										if(nrf_send[2]==1)
											OLED_ShowNum(102,5,--pid.Y_kd,3,12);

										else
											OLED_ShowNum(102,5,++pid.Y_kd,3,12);
									
							}
							break;
						case 4:
							key.canshu_jin=0;
							if(key.canshu_weihzi<6)
							{
								OLED_ShowString(0,key.canshu_weihzi,">",12);
							}
							else
							{
								OLED_ShowString(64,key.canshu_weihzi-6,">",12);
							}
							break;
					}
					break;
			}
			break;
	}
}
void buchang_menu(void)
{
	OLED_ShowString(20,2,"Pitch:",16);
	OLED_ShowString(20,4,"Roll:",16);
	OLED_ShowString(10,key.buchang_weizhi,">",16);
	
	OLED_ShowNum(72,2,pid.P_buhang,3,16);
	OLED_ShowNum(72,4,pid.R_buhang,3,16);
}
void buchang_debug(void)
{
	switch(key.buchang_jin)
	{
		case 0:
			switch(key.key_value)
			{
				case 1:
					key.buchang_weizhi-=2;
					if(key.buchang_weizhi<2)key.buchang_weizhi=4;
					OLED_ShowString(10,2," ",16);
					OLED_ShowString(10,4," ",16);
					OLED_ShowString(10,key.buchang_weizhi,">",16);
					break;
				case 2:
					key.buchang_weizhi+=2;
					if(key.buchang_weizhi>4)key.buchang_weizhi=2;
					OLED_ShowString(10,2," ",16);
					OLED_ShowString(10,4," ",16);
					OLED_ShowString(10,key.buchang_weizhi,">",16);
					break;
				case 3:
					key.buchang_jin=1;
					OLED_ShowString(10,key.buchang_weizhi,"<",16);
					break;
				case 4:
					key.zhu_jin=0;
					OLED_Clear();
					xianshi_zhu();
					break;
			}
			break;
		case 1:
			switch(key.buchang_weizhi)
			{
				case 2:
					switch(key.key_value)
					{
						case 1:
							nrf_send[2]=2;
							break;
						case 2:
							nrf_send[2]=1;
							break;
						case 3:
							//发送后显示
							nrf_send[1]=2;
							nrf_send[2]=12;
							if(NRF24L01_TxPacket(nrf_send)==TX_OK)
							{  
								LED1=!LED1;
										if(nrf_send[2]==1)
											OLED_ShowNum(72,2,--pid.P_buhang,3,16);

										else
											OLED_ShowNum(72,2,++pid.P_buhang,3,16);
							}
							break;
						case 4:
							key.buchang_jin=0;
							OLED_ShowString(10,key.buchang_weizhi,">",16);
							break;
					}
					break;
				case 4:
					switch(key.key_value)
					{
						case 1:
							nrf_send[2]=2;
							break;
						case 2:
							nrf_send[2]=1;
							break;
						case 3:
							//发送后显示
							nrf_send[1]=2;
							nrf_send[2]=13;
							if(NRF24L01_TxPacket(nrf_send)==TX_OK)
							{  
								LED1=!LED1;
										if(nrf_send[2]==1)
											OLED_ShowNum(72,4,--pid.R_buhang,3,16);

										else
											OLED_ShowNum(72,4,++pid.R_buhang,3,16);
							}
							break;
						case 4:
							key.buchang_jin=0;
							OLED_ShowString(10,key.buchang_weizhi,">",16);
							break;
					}
					break;
			}
			break;
	}
}
void zhu_chang(void)
{
	switch(key.zhu_jin)
	{
		case 0:
					switch(key.key_value)
					{
						case 2:
							key.zhu_weizhi+=2;
							if(key.zhu_weizhi>6) key.zhu_weizhi=2;
							OLED_ShowString(2,2," ",16);
							OLED_ShowString(2,4," ",16);
							OLED_ShowString(2,6," ",16);
							OLED_ShowString(2,key.zhu_weizhi,">",16);
							break;
						case 1:
							key.zhu_weizhi-=2;
							if(key.zhu_weizhi<2) key.zhu_weizhi=6;
							OLED_ShowString(2,2," ",16);
							OLED_ShowString(2,4," ",16);
							OLED_ShowString(2,6," ",16);
							OLED_ShowString(2,key.zhu_weizhi,">",16);
							break;
						case 3:
							key.zhu_jin=1;
							switch(key.zhu_weizhi)
							{
								case 2:
									OLED_Clear();
									canshu_menu();
									break;
								case 4:
									//补偿调节
								  OLED_Clear();
									buchang_menu();
									break;
								case 6:
									OLED_Clear();
									fly_menu();
									break;
							}
							break;
					}
			break;
		case 1:
				switch(key.zhu_weizhi)
				{
					case 2:
						canshu_debug();
						break;
					case 4:
						buchang_debug();
						break;
					case 6:
						fly_menu();
						break;
				}
			break;
	}
}
//第一个按键
void EXTI2_IRQHandler(void)
{
	delay_ms(20);//消抖
	if(KEY1==0)
	{
		key.key_value=1;
		if(key.zhu==2)
		{
			LED4=!LED4;
			OLED_Clear();
			xianshi_zhu();
			key.zhu=0;
		}
		delay_ms(10);
		while(!KEY1);
	}
	switch(key.zhu)
	{
		case 0:
			zhu_chang();
			break;
		case 1:
			break;
	}
	
	EXTI_ClearITPendingBit(EXTI_Line2); //清除LINE0上的中断标志位  
}
void EXTI15_10_IRQHandler(void) 
{
	delay_ms(20);//消抖
	if(KEY2==0)
	{
		key.key_value=2;
		LED4=!LED4;
		delay_ms(10);
		while(!KEY2);
	}
	if(KEY3==0)
	{
		key.key_value=3;
		LED4=!LED4;
		delay_ms(10);
		while(!KEY3);
	}
	if(KEY4==0)
	{
		key.key_value=4;
		LED4=!LED4;
		delay_ms(10);
		while(!KEY4);
	}
	switch(key.zhu)
	{
		case 0:
			zhu_chang();
			break;
		case 1:
			break;
	}
	EXTI_ClearITPendingBit(EXTI_Line13); //清除LINE0上的中断标志位 
	EXTI_ClearITPendingBit(EXTI_Line14); //清除LINE0上的中断标志位  
	EXTI_ClearITPendingBit(EXTI_Line15); //清除LINE0上的中断标志位 
}


















