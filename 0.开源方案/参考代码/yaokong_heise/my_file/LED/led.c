#include "led.h"

int led_init(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;
	//使能gpio时钟
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA|RCC_APB2Periph_GPIOB|RCC_APB2Periph_GPIOC,ENABLE);
	//初始化gpio
	GPIO_InitStruct.GPIO_Mode= GPIO_Mode_Out_PP; //推挽输出
	GPIO_InitStruct.GPIO_Pin=GPIO_Pin_0|GPIO_Pin_3;
	GPIO_InitStruct.GPIO_Speed=GPIO_Speed_50MHz;  
	GPIO_Init(GPIOA,&GPIO_InitStruct);
	
	GPIO_InitStruct.GPIO_Pin=GPIO_Pin_1;
	GPIO_Init(GPIOB,&GPIO_InitStruct);
	
	GPIO_InitStruct.GPIO_Pin=GPIO_Pin_3;
	GPIO_Init(GPIOC,&GPIO_InitStruct);
	//灭灯
	LED1=1;
	LED2=1;
	LED3=1;
	LED4=1;
	
	
	return 0;
}

