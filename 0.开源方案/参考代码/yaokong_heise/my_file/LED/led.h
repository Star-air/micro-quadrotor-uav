#ifndef _led
#define _led
#include "sys.h"


#define LED1 PBout(1)
#define LED2 PAout(3)
#define LED3 PAout(0)
#define LED4 PCout(3)

int led_init(void);


#endif


