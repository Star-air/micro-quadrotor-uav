#include "time.h"

/*
定时器初始化
入口参数：中断时间 
单位：ms
*/
int time3_init(int inter)
{
	TIM_TimeBaseInitTypeDef TIM_TimeBase_time3;
	NVIC_InitTypeDef NVI_InitStructure;	
	
	inter=inter*10;
	
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3,ENABLE);
	
	TIM_TimeBase_time3.TIM_ClockDivision= TIM_CKD_DIV1;
	TIM_TimeBase_time3.TIM_CounterMode=  TIM_CounterMode_Up;  // 向上计数
	TIM_TimeBase_time3.TIM_Period= inter;//阈值 
	TIM_TimeBase_time3.TIM_Prescaler= 7199;
	TIM_TimeBaseInit(TIM3,&TIM_TimeBase_time3);
	
	TIM_ITConfig(TIM3,TIM_IT_Update,ENABLE ); //使能指定的TIM3中断,允许更新中断
	
	//中断优先级NVIC设置
	NVI_InitStructure.NVIC_IRQChannel = TIM3_IRQn;  //TIM3中断
	NVI_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;  //先占优先级0级
	NVI_InitStructure.NVIC_IRQChannelSubPriority = 3;  //从优先级3级
	NVI_InitStructure.NVIC_IRQChannelCmd = ENABLE; //IRQ通道被使能
	NVIC_Init(&NVI_InitStructure);  //初始化NVIC寄存器
	
	
	TIM_Cmd(TIM3, ENABLE);  //使能TIMx	
	
	return 0;
	
	
}









