#include "sys.h"
#include "head.h"
#include "key.h"
/*******************************************************
前后  Get_Adc_Average(11,5);
左右  Get_Adc_Average(10,5);
油门 Get_Adc_Average(2,5);
旋转 Get_Adc_Average(1,5);

*******************************************************/
/*
发送的数组
0：0X12 校验码
1：起飞标志      1：可以起飞 0：不可起飞 2:参数调节
2：前后方向控制  0：停止 1：后  2：前
3：左右方向控制  0：停止 1：左  2：右
4：油门控制      0：停止 1：下降 2：上升
5：旋转控制      0：停止 1：逆时针 2：顺时针
*/
u8 nrf_send[6];
void xianshi_fly(void);

void check_main(void);
//按键参数
extern key_type key;
//接收信号标志
u8 jieshou_error=0;
//摇杆电位器参数
extern yaogan yaogan_ad;
//通用参数
extern tongyong_type tongyong;
/*
起飞标志
值为0 可以起飞
    1 初始化未完成
*/
u8 jishen_error=0;

int main(void)  
{
	u16 t=0;
	u8 tmp_buf_rec[4];
	nrf_send[1]=0;  //不可起飞 定时器开启后可以起飞
	nrf_send[0]=0x12;
	delay_init();//初始化延时函数
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2); //设置NVIC中断分组2:2位抢占优先级，2位响应优先级
	led_init();  //GPIO初始化
	ad_init();   //AD初始化
	key_init();
	OLED_Init();
	OLED_Clear();
	canshu_chushihua();
	OLED_ShowString(20,3,"WAIT...",16);
	NRF24L01_Init(); //NRF24L01初始化
	while(NRF24L01_Check())//检测NRF24L01是否存在
	{

		OLED_ShowString(20,5,"NRF_ERROR",16);
	}
	OLED_ShowString(20,5,"         ",16);
	//等待接收机身初始化结果
	NRF24L01_RX_Mode();  //接收模式
	delay_ms(3000);
	while(NRF24L01_RxPacket(tmp_buf_rec))//一旦接收到信息,则显示出来.
	{
		delay_ms(1);
		t++;
		if(t>1000) break;
		
	}
	tongyong.dianya_jishen=tmp_buf_rec[2]+256;
	//机身电压显示
	OLED_ShowNum(8*10,0,tongyong.dianya_jishen/100,1,16);
	OLED_ShowString(8*11,0,".",16);
	OLED_ShowNum(8*12,0,tongyong.dianya_jishen%100,2,16);
	OLED_ShowString(0,0,"fus power:",16);
	//机身状态返回
	if(tmp_buf_rec[3]==0)
	{
		OLED_ShowString(20,5,"FUS OK!",16);
	}
	else if(tmp_buf_rec[3]==1)
	{
		jishen_error=1;
		OLED_ShowString(20,5,"MPU ERROR!",16);
	}
	else if(tmp_buf_rec[3]==2)
	{
		jishen_error=1;
		OLED_ShowString(20,5,"NRF ERROR!",16);
	}
	else if(tmp_buf_rec[3]==3)
	{
		jishen_error=1;
		OLED_ShowString(20,5,"POWER LOW!",16);
	}
	if(jieshou_error==1)
	{
		OLED_ShowString(20,5,"REC ERROR!",16);
	}
	NRF24L01_TX_Mode();  //接收模式
	/***********此处需改***************/
	//主函数循环函数
	check_main();
}
void check_main(void)
{
	int ceshi_x0,ceshi_y0,ceshi_x1,ceshi_y1;
	u8 t=0;
	OLED_ShowString(20,3,"INIT OK!",16);
	while(1)
	{
		if(nrf_send[1]!=2)   //发送函数
		{
			if(NRF24L01_TxPacket(nrf_send)==TX_OK)
			{
				LED1=!LED1;
			}
			else
			{
				LED3=!LED3;
			}
		}
		//起飞操作：两摇杆拉至最低保持一秒
		if(nrf_send[1]==0)
		{
			
			ceshi_x0=Get_Adc_Average(10,5)//油门
			ceshi_y0=Get_Adc_Average(2,5);//前后
			if(ceshi_x0<100&&ceshi_y0<100)
			{
				delay_ms(10);
				ceshi_x1=Get_Adc_Average(10,5);//油门
				ceshi_y1=Get_Adc_Average(2,5);//前后
				if(ceshi_y1<100&&ceshi_x1<100)
				{
					t++;
					if(jishen_error==0&&t>10)
					{
						OLED_Clear();
						nrf_send[1]=1;  //可以起飞
						xianshi_fly();
						time3_init(10);
					}
				}
				else
				{
					t=0;
				}
			}
			
			
		}
	}
}










void xianshi_fly(void)
{

	OLED_ShowCHinese(85,0,0);  //上
	OLED_ShowCHinese(85,4,1);  //下
	OLED_ShowCHinese(69,2,2);   //左
	OLED_ShowCHinese(101,2,3);   //右

	OLED_ShowString(0,0,"Y:",16);
	OLED_ShowString(0,4,"X:",16);
}



















